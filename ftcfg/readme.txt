ftcfg is the file to store the spk basic params for speaker calibration
the file name format is
[chip name]-[spk number].ftcfg
eg:
tas2563-a.ftcfg
tas2563-b.ftcfg
tas2781-a.ftcfg
the file name is defined in tasdevice.c, see 
static const char *ftcfg[] = {
	"%s-a.ftcfg",
	"%s-b.ftcfg",
	"%s-c.ftcfg",
	"%s-d.ftcfg"
};

Copy ftcfg file to the TASDEVICE_CAL_CFG_PATH defined in tasdevice-ftc.h.