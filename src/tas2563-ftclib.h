/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2023 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef TAS2563_LIB_FTC_H_
#define TAS2563_LIB_FTC_H_

#define TAS2563_RUNTIME_RE_REG		TASDEVICE_REG(0x64, 0x02, 0x48)
#define TAS2563_PRM_R0_REG		TASDEVICE_REG(0x00, 0x0f, 0x34)
#define TAS2563_PRM_R0_LOW_REG	TASDEVICE_REG(0x00, 0x0f, 0x48)
#define TAS2563_PRM_INVR0_REG		TASDEVICE_REG(0x00, 0x0f, 0x40)
#define TAS2563_PRM_POW_REG	TASDEVICE_REG(0x00, 0x0d, 0x3c)
#define TAS2563_PRM_TLIMIT_REG	TASDEVICE_REG(0x00, 0x10, 0x14)
#define TAS2563_PRM_PLT_FLAG_REG	TASDEVICE_REG(0x00, 0x0d, 0x74)
#define TAS2563_PRM_SINEGAIN_REG	TASDEVICE_REG(0x00, 0x0d, 0x7c)

struct tas2563_calibrated_data {
	uint32_t mprm_EnFF;
	uint32_t mprm_DisTck;
	uint32_t mprm_TE_SCThr;
	uint32_t mTE_TA1;
	uint32_t mTE_TA1_AT;
	uint32_t mTE_TA2;
	uint32_t mTE_AT;
	uint32_t mTE_DT;
};

void tas2563_ftc_start(struct TFTCConfiguration *ftc, int spkno);
double tas2563_get_re(struct TFTCConfiguration *ftc);
void tas2563_ftc_stop(struct TFTCConfiguration *ftc, int spkno);
int tas2563_get_Re_deltaT(struct TFTCConfiguration *ftc,
	double nPPC3_alpha, double *pnRe, double *pnDeltaT);
void tas2563_get_keypara(struct TFTCConfiguration *ftc);
uint32_t tas2563_calc_prm_tlimit(struct TFTCConfiguration *ftc,
	struct TSPKCharData *spk);

#endif
