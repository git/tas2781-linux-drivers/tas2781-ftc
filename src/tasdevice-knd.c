/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2023 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/errno.h>
#include <unistd.h>

#include "tasdevice-common.h"

int wr_nd(const char *kcontrol, const char *node, const char *value)
{
	int fd;
	int ret;
	char kcon_node[PATH_MAX];

	snprintf(kcon_node, sizeof(kcon_node), "%s/%s", kcontrol, node);

	if (access (kcon_node, F_OK) != 0) {
		LOGE("ERROR:%s not exist!\n", kcon_node);
		ret = -errno;
		goto error;
	}

	fd = open(kcon_node, O_WRONLY);
	if (fd < 0) {
		LOGE("open %s failed, errno = %d", kcon_node, errno);
		ret = -errno;
		goto error;
	}

	ret = write(fd, value, strlen(value) + 1);
	if (ret == -1) {
		ret = -errno;
	} else if (ret != strlen(value) + 1) {
		/* even though EAGAIN is an errno value that could be set
		in some cases, none of them apply here.  So, this return
		value can be clearly identified when debugging */
		LOGE("return len = %d\n", ret);
		ret = -EAGAIN;
	} else {
		ret = 0;
	}

	errno = 0;
	close(fd);
error:
	return ret;
}

int rd_nd(const char *kcontrol, const char *node, char *value, int len)
{
	int fd;
	int ret;
	char kcon_node[PATH_MAX];

	snprintf(kcon_node, sizeof(kcon_node), "%s/%s", kcontrol, node);
	if (access (kcon_node, F_OK) != 0) {
		LOGE("ERROR: %s not exist!\n", kcon_node);
		ret = -errno;
		goto error;
	}

	fd = open(kcon_node, O_RDWR);
	if (fd < 0) {
		LOGE("open %s failed, errno = %d", kcon_node, errno);
		ret = -errno;
		goto error;
	}

	ret = read(fd, value, len);
	if (ret == -1) {
		ret = -errno;
	}

	errno = 0;
	close(fd);
error:
	return ret;
}
