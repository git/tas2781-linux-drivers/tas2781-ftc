/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2024 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#ifndef TASDEVICE_FTC_H_
#define TASDEVICE_FTC_H_

#define TASDEVICE_CAL_TXT	("/home/audio/ti/tasdevice-cal.txt")
//#define TAS2781_CAL_BIN	("/mnt/vendor/persist/audio/%s_cal_0x%02x.bin")
//#define TAS2781_CAL_BIN_PATH	("/mnt/vendor/persist/audio/")
#define TASDEVICE_CAL_BIN	("%s%s-0x%02x-cal.bin")
#define TASDEVICE_CAL_BIN_PATH	("/system/vendor/firmware/")
#define TASDEVICE_CAL_CFG_PATH	("/lib/firmware/")

struct TSPKCharData {
	double nSpkTMax;
	double nSpkReTolPer;
	double nSpkReAlpha;

	double nPPC3_Re0;
	double nPPC3_FWarp;
	double nPPC3_Bl;
	double nPPC3_Mms;
	double nPPC3_RTV;
	double nPPC3_RTM;
	double nPPC3_RTVA;
	double nPPC3_SysGain;
	double nPPC3_DevNonlinPer;
	double nPPC3_PIG;

	double nReHi;
	double nReLo;
	double nf0Hi;
	double nf0Lo;
	double nQHi;
	double nQLo;

	uint32_t mprm_Plt_Flag;
	uint32_t mprm_SineGain;

	unsigned char nDevAddr;
	unsigned char mCoeff_dev_id;
	void *tasdev_calibrated_data;
};

struct TFCTBinFile {
	FILE *mBinFile;
	unsigned int mnBinIndex;
	char *pBin;
	char *pDevBlock;
	unsigned int mnDevBlockIndex;
};

struct TFCTResult {
	unsigned char mCalbirationResult;
	uint32_t dev_prm_pow;	// Total RMS power coefficient
	uint32_t dev_prm_tlimit;	// Delta temperature limit coefficient
	double dev_re;
	bool bWrittenintoFile;
};

struct TFTCConfiguration {
	bool bVerbose;
	bool bLoadCalibration;
	bool bSPIEnable;
	unsigned int nCalibrationTime;
	unsigned char dev_name[32];
	unsigned char spkvendorid;
	unsigned char cal_count;
	unsigned char dev_idx;
	double nPPC3_FS;
	int mTILoad;
	double temp_cal;
	unsigned char nActiveSpk_num;
	struct TFCTBinFile mtCalBin;
	struct TFCTResult *mnTFctResult;
	struct TSPKCharData *nTSpkCharDev;
	void (*ftc_start)(struct TFTCConfiguration *ftc,
		int spkno);
	double (*get_re)(struct TFTCConfiguration *ftc);
	uint32_t (*calc_prm_tlimit)(struct TFTCConfiguration *ftc,
		struct TSPKCharData *spk);
	void (*ftc_stop)(struct TFTCConfiguration *ftc, int spkno);
	int (*get_Re_deltaT)(struct TFTCConfiguration *ftc,
		double nPPC3_alpha, double *pnRe, double *pnDeltaT);
	void (*get_keypara)(struct TFTCConfiguration *ftc);
	int (*ftcfg_version_check)(struct TFTCConfiguration *ftc);
	unsigned int r0_reg;
	unsigned int r0_low_reg;
	unsigned int invr0_reg;
	unsigned int pow_reg;
	unsigned int tlimit_reg;
	unsigned int prm_Plt_Flag_reg;
	unsigned int prm_SineGain_reg;
	/* Only for tas2781 */
	unsigned int prm_SineGain2_reg;
	unsigned int prm_Thr_reg;
};

#endif /* TASDEVICE_FTC_H_ */
