LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := tasdevice-interface.c tasdevice.c tas2563-ftc.c tas2781-ftc.c tasdevice-knd.c

$(warning "value of -- boardid TARGET_PRODUCT: $(TARGET_PRODUCT)")

LOCAL_SHARED_LIBRARIES := libc libcutils

#LOCAL_MODULE_PATH := $(TARGET_OUT)/bin
LOCAL_MODULE := tasdevice-ftc
LOCAL_MODULE_TAGS := optional

##############
LOCAL_MODULE_OWNER := TI
LOCAL_PROPRIETARY_MODULE := true

include $(BUILD_EXECUTABLE)
