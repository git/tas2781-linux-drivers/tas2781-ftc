/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2023 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "tasdevice-ftc.h"
#include "tasdevice.h"
#include "tasdevice-common.h"
#include "tas2781-ftclib.h"

#define TAS2781_PRM_CLK_CFG_REG		TASDEVICE_REG(0x00, 0x00, 0x5c)
#define TAS2781_PRM_INT_MASK_REG	TASDEVICE_REG(0x00, 0x00, 0x3b)
#define TAS2781_PRM_RSVD_REG		TASDEVICE_REG(0x00, 0x01, 0x19)
#define TAS2781_PRM_TEST_57_REG		TASDEVICE_REG(0x00, 0xfd, 0x39)
#define TAS2781_PRM_TEST_62_REG		TASDEVICE_REG(0x00, 0xfd, 0x3e)
#define TAS2781_PRM_PVDD_UVLO_REG	TASDEVICE_REG(0x00, 0x00, 0x71)
#define TAS2781_PRM_CHNL_0_REG		TASDEVICE_REG(0x00, 0x00, 0x03)
#define TAS2781_PRM_NG_CFG0_REG		TASDEVICE_REG(0x00, 0x00, 0x35)
#define TAS2781_PRM_IDLE_CH_DET_REG	TASDEVICE_REG(0x00, 0x00, 0x66)

#define TAS2781_TEST_UNLOCK_REG		TASDEVICE_REG(0x00, 0xFD, 0x0D)
#define TAS2781_TEST_PAGE_UNLOCK	0x0D

void tas2781_get_keypara(struct TFTCConfiguration *ftc)
{
	// PTG = 0.8 (prm_SineGain)
	uint32_t mprm_Plt_Flag = tasdevice_coeff_read(ftc,
		ftc->prm_Plt_Flag_reg);
	// PTG = 0.8 (prm_SineGain2)
	uint32_t mprm_SineGain = tasdevice_coeff_read(ftc,
		ftc->prm_SineGain_reg);
	uint32_t mprm_SineGain2 = tasdevice_coeff_read(ftc,
		ftc->prm_SineGain2_reg);

	LOGI("mprm_Plt_Flag:0X%08X\n", mprm_Plt_Flag);
	LOGI("mprm_SineGain:0X%08X\n", mprm_SineGain);
	LOGI("mprm_SineGain2:0X%08X\n", mprm_SineGain2);
}

void tas2781_calculate_sinegain(uint32_t *prm_sinegain,
	uint32_t *prm_sinegain2, double nrehi, double nppc3_sysgain)
{
	/* 1.414*1.05*Re_max*sqrt(40mW/4ohms) */
	double pilot_lvl = 1.414*1.05*nrehi*0.1;
	double pilot_g = pilot_lvl/nppc3_sysgain;
	double prm_sinegain2_d = pilot_g/32;
	double prm_sinegain_d = pilot_g;

	*prm_sinegain = (uint)(prm_sinegain_d * (0x80000000));
	*prm_sinegain2 = (uint)(prm_sinegain2_d * (0x80000000));
}

void tas2781_ftc_start(struct TFTCConfiguration *ftc, int spkno)
{
	struct TSPKCharData *spk = &(ftc->nTSpkCharDev[spkno]);
	struct tas2781_calibrated_data *p = spk->tasdev_calibrated_data;
	uint32_t prm_SineGain2, prm_SineGain;
	uint32_t prm_Plt_Flag = 0x40000000;

	p->prm_int_msk = tasdevice_byte_read(ftc, TAS2781_PRM_INT_MASK_REG);
	p->mprm_CLK_CFG = tasdevice_byte_read(ftc, TAS2781_PRM_CLK_CFG_REG);
	p->mprm_Rsvd = tasdevice_byte_read(ftc, TAS2781_PRM_RSVD_REG);
	tasdevice_byte_write(ftc,TAS2781_TEST_UNLOCK_REG,
		TAS2781_TEST_PAGE_UNLOCK);
	p->mprm_testPage57 = tasdevice_byte_read(ftc,
		TAS2781_PRM_TEST_57_REG);
	tasdevice_byte_write(ftc, TAS2781_TEST_UNLOCK_REG,
		TAS2781_TEST_PAGE_UNLOCK);
	p->mprm_testPage62 = tasdevice_byte_read(ftc,
		TAS2781_PRM_TEST_62_REG);
	p->mprm_PVDD_UVLO = tasdevice_byte_read(ftc,
		TAS2781_PRM_PVDD_UVLO_REG);
	p->mprm_CHNL_0 = tasdevice_byte_read(ftc, TAS2781_PRM_CHNL_0_REG);
	p->mprm_NG_CFG0 = tasdevice_byte_read(ftc, TAS2781_PRM_NG_CFG0_REG);
	p->mprm_IDLE_CH_DETECT = tasdevice_byte_read(ftc,
		TAS2781_PRM_IDLE_CH_DET_REG);

	// mask for over current interrupt.
	tasdevice_byte_write(ftc, TAS2781_PRM_INT_MASK_REG, 0xfe);
	tasdevice_byte_write(ftc, TAS2781_PRM_CLK_CFG_REG, 0xdd);
	tasdevice_byte_write(ftc, TAS2781_PRM_RSVD_REG, 0x20);
	tasdevice_byte_write(ftc,
		TAS2781_TEST_UNLOCK_REG, TAS2781_TEST_PAGE_UNLOCK);
	tasdevice_byte_write(ftc, TAS2781_PRM_TEST_57_REG, 0x14);
	tasdevice_byte_write(ftc,
		TAS2781_TEST_UNLOCK_REG, TAS2781_TEST_PAGE_UNLOCK);
	tasdevice_byte_write(ftc, TAS2781_PRM_TEST_62_REG, 0x45);
	tasdevice_byte_write(ftc, TAS2781_PRM_PVDD_UVLO_REG, 0x3);
	tasdevice_byte_write(ftc, TAS2781_PRM_CHNL_0_REG, 0xA8);
	tasdevice_byte_write(ftc, TAS2781_PRM_NG_CFG0_REG, 0xB9);
	tasdevice_byte_write(ftc, TAS2781_PRM_IDLE_CH_DET_REG, 0x92);

	if (ftc->prm_Thr_reg)
		p->prm_Thr_val = tasdevice_coeff_read(ftc, ftc->prm_Thr_reg);

	spk->mprm_Plt_Flag = tasdevice_coeff_read(ftc,
		ftc->prm_Plt_Flag_reg);
	LOGI("mprm_Plt_Flag:0X%08X\n", spk->mprm_Plt_Flag);
	// PTG = 0.8 (prm_SineGain)
	spk->mprm_SineGain = tasdevice_coeff_read(ftc,
		ftc->prm_SineGain_reg);
	LOGI("mprm_SineGain:0X%08X\n", spk->mprm_SineGain);
	// PTG = 0.8 (prm_SineGain2)
	p->mprm_SineGain2 = tasdevice_coeff_read(ftc,
		ftc->prm_SineGain2_reg);
	LOGI("mprm_SineGain2:0X%08X\n", p->mprm_SineGain2);

	tas2781_calculate_sinegain(&prm_SineGain, &prm_SineGain2,
		12, spk->nPPC3_SysGain);
	if (ftc->prm_Thr_reg)
		tasdevice_coeff_write(ftc, ftc->prm_Thr_reg, 0x56); // 1mA
	// PE Enable = On (prm_Plt_Flag)
	tasdevice_coeff_write(ftc, ftc->prm_Plt_Flag_reg,
		prm_Plt_Flag);
	// PTG = 0.8 (prm_SineGain) peak -22dBfs
	tasdevice_coeff_write(ftc, ftc->prm_SineGain_reg, prm_SineGain);
	tasdevice_coeff_write(ftc, ftc->prm_SineGain2_reg, prm_SineGain2);
}

void tas2781_ftc_stop(struct TFTCConfiguration *ftc, int spkno)
{
	struct TSPKCharData *spk = &(ftc->nTSpkCharDev[spkno]);
	struct tas2781_calibrated_data *p = spk->tasdev_calibrated_data;

	if (ftc->prm_Thr_reg)
		tasdevice_coeff_write(ftc, ftc->prm_Thr_reg, p->prm_Thr_val);
	// PE Enable(prm_Plt_Flag)
	tasdevice_coeff_write(ftc, ftc->prm_Plt_Flag_reg,
		spk->mprm_Plt_Flag);
	// PTG (prm_SineGain)
	tasdevice_coeff_write(ftc, ftc->prm_SineGain_reg,
		spk->mprm_SineGain);
	// PTG (prm_SineGain2)
	tasdevice_coeff_write(ftc, ftc->prm_SineGain2_reg,
		p->mprm_SineGain2);

	// unmask for over current interrupt, 0xfc
	tasdevice_byte_write(ftc, TAS2781_PRM_INT_MASK_REG, p->prm_int_msk);
	tasdevice_byte_write(ftc, TAS2781_PRM_CLK_CFG_REG, p->mprm_CLK_CFG);
	tasdevice_byte_write(ftc, TAS2781_PRM_RSVD_REG, p->mprm_Rsvd);
	tasdevice_byte_write(ftc, TAS2781_TEST_UNLOCK_REG,
		TAS2781_TEST_PAGE_UNLOCK);
	tasdevice_byte_write(ftc, TAS2781_PRM_TEST_57_REG, p->mprm_testPage57);
	tasdevice_byte_write(ftc,
		TAS2781_TEST_UNLOCK_REG, TAS2781_TEST_PAGE_UNLOCK);
	tasdevice_byte_write(ftc,
		TAS2781_PRM_TEST_62_REG,  p->mprm_testPage62);
	tasdevice_byte_write(ftc,
		TAS2781_PRM_PVDD_UVLO_REG, p->mprm_PVDD_UVLO);
	tasdevice_byte_write(ftc,
		TAS2781_PRM_CHNL_0_REG, p->mprm_CHNL_0);
	tasdevice_byte_write(ftc,
		TAS2781_PRM_NG_CFG0_REG, p->mprm_NG_CFG0);
	tasdevice_byte_write(ftc,
		TAS2781_PRM_IDLE_CH_DET_REG, p->mprm_IDLE_CH_DETECT);
}

// ----------------------------------------------------------------------------
// get_re
// ----------------------------------------------------------------------------
// Description:
//	  Used to measure actual Re from the speaker.
//
// Inputs:
//	  gpFTCC->nPPC3_Re0 - Nominal Re obtained during characterization.
// ----------------------------------------------------------------------------
double tas2781_get_re(struct TFTCConfiguration *ftc)
{
	uint32_t prm_R0, prm_SigFlag, prm_latch_IR0;
	double re;

	prm_latch_IR0 = tasdevice_byte_read(ftc,
		TAS2781_RUNTIME_LATCH_RE_REG);

	LOGI("prm_latch_IR0: 0x%02X\n", prm_latch_IR0);
	if ((prm_latch_IR0 & 2) == 2) {
		re = 0.0;
	} else {
		//Actual Re
		prm_SigFlag = tasdevice_coeff_read(ftc,
			TAS2781_RUNTIME_RE_REG_TF);
		prm_R0 = tasdevice_coeff_read(ftc, TAS2781_RUNTIME_RE_REG);
		re = coeff_fixed_to_float(prm_R0);
		if (((prm_SigFlag & 0x80000000) == 0x80000000) &&
			((prm_SigFlag & 0x7fffffff) <= 0x1260))
			re = 100;
	}
	return re;
}

int tas2781_get_Re_deltaT(struct TFTCConfiguration *ftc,
	double nPPC3_alpha, double *pnRe, double *pnDeltaT)
{
	double r0, Re_rt, Re_cal;
	int prm_R0, prm_R0_rt;
	int nResult = 0;

	if (!pnRe && !pnDeltaT) {
		nResult = -1;
		goto end;
	}

	if (!nPPC3_alpha) {
		nResult = -2;
		goto end;
	}

	prm_R0 = tasdevice_coeff_read(ftc, TAS2781_PRM_R0_REG);
	r0 = (double)prm_R0 / 0x80000000;
	Re_cal = r0 * 16;
	if (Re_cal == 0.0) {
		nResult = -3;
		goto end;
	}

	prm_R0_rt = tasdevice_coeff_read(ftc, TAS2781_RUNTIME_RE_REG);
	Re_rt = coeff_fixed_to_float(prm_R0_rt);

	if (pnRe)
		*pnRe = Re_rt;


	if (pnDeltaT)
		*pnDeltaT = (Re_rt - Re_cal) / (Re_cal * nPPC3_alpha);

end:
	return nResult;
}

uint32_t tas2781_calc_prm_tlimit(struct TFTCConfiguration *ftc,
	struct TSPKCharData *spk)
{
	struct tas2781_calibrated_data *p = spk->tasdev_calibrated_data;
	double nPPC3_TH_NONLIN_PER = p->nPPC3_TH_NONLIN_PER;
	double delta_t_max = spk->nSpkTMax - ftc->temp_cal;
	double nDevNonlinPer = spk->nPPC3_DevNonlinPer;
	double nPPC3_DC_RES_PER = p->nPPC3_DC_RES_PER;
	double nPPC3_TH_RES_PER = p->nPPC3_TH_RES_PER;
	double nPPC3_PIG_NEW = spk->nPPC3_PIG;
	double alpha = spk->nSpkReAlpha;
	// Thermal controller
	double DC_Res_Margin, Thermal_NonLin_Margin, Thermal_Res_Margin;
	double TC_Temp_Device_NonLin, TC_TLimit;
	uint32_t prm_TLimit;

	// Calculation to Thermal Limit
	TC_Temp_Device_NonLin = floor(((1 / alpha) + delta_t_max) *
		(nDevNonlinPer / 100));
	DC_Res_Margin = floor((nPPC3_DC_RES_PER / 100) *
		(delta_t_max + (1 / alpha)));
	// 3. Speaker Thermal Non-linearity
	Thermal_NonLin_Margin = floor(((nPPC3_TH_NONLIN_PER/100) *
		delta_t_max));
	// 4. Speaker Thermal Resistance Modelling error
	Thermal_Res_Margin = floor((((nPPC3_TH_RES_PER / 100)/
		(1 + (nPPC3_TH_RES_PER / 100))) / (2 * nPPC3_PIG_NEW *
		(1 / (1 + (nPPC3_TH_RES_PER / 100))) + 1)) * delta_t_max);

	TC_TLimit = delta_t_max - TC_Temp_Device_NonLin - DC_Res_Margin -
		Thermal_NonLin_Margin - Thermal_Res_Margin;

	TC_TLimit = TC_TLimit / 256;
	if (TC_TLimit >= 1)
		prm_TLimit = 0x7FFFFFFF;
	else
		prm_TLimit = TC_TLimit * 0x80000000;

	return prm_TLimit;
}

int tas2781_ftcfg_version_check(struct TFTCConfiguration *ftc)
{
	struct TSPKCharData *pSpk = ftc->nTSpkCharDev;
	char check[TASDEVICE_MAX_CHANNELS], tmp;
	int i, ret = 0;

	if (ftc->nActiveSpk_num > TASDEVICE_MAX_CHANNELS) {
		LOGE("%s: spk number is more than the correct value!\n",
			__func__);
		return -1;
	}

	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		struct tas2781_calibrated_data *p =
			pSpk[i].tasdev_calibrated_data;
		char member[4];

		member[0] = p->prm_Plt_Flag[1] | p->prm_Plt_Flag[2];
		member[1] = p->prm_SineGain[1] | p->prm_SineGain[2];
		member[2] = p->prm_SineGain2[1] | p->prm_SineGain2[2];
		member[3] = p->prm_Thr[1] | p->prm_Thr[2];

		if ((member[0] | member[1] | member[2] | member[3]) == 0) {
			check[i] = 0;
			continue;
		}

		if ((member[0] && member[1] && member[2] && member[3]) != 0) {
			check[i] = 1;
			continue;
		}
		ret = -1;
		goto out;
	}

	tmp = check[0];
	i = 1;
	while(i < ftc->nActiveSpk_num) {
		tmp &= check[i];
		i++;
	}

	if(tmp == 1) {
		ret = 1;
		goto out;
	}

	tmp = check[0];
	i = 1;
	while(i < ftc->nActiveSpk_num) {
		tmp |= check[i];
		i++;
	}

	if (tmp != 0) {
		LOGE("%s: ftcfg versions is in mess\n", __func__);
		ret = -1;
	}
out:
	return ret;
}
