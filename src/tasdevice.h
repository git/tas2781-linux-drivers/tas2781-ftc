/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2024 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef TASDEVICE_H_
#define TASDEVICE_H_

#define TILOAD_NODE ("/dev/tiload_node")

/* 0000 0000 0BBB BBBB BPPP PPPP PRRR RRRR */

#define TASDEVICE_REG(book, page, reg)	((((unsigned int)book * 256 * 128) + \
	((unsigned int)page * 128)) + reg)

#define TASDEVICE_BOOK_ID(reg)	((unsigned char)(reg / (256 * 128)))
#define TASDEVICE_PAGE_ID(reg)	((unsigned char)((reg % (256 * 128)) / 128))
#define TASDEVICE_PAGE_REG(reg)	((unsigned char)((reg % (256 * 128)) % 128))

#define TILOAD_IOC_MAGIC	(0xE0)

#define TILOAD_IOCTL_SET_CHL		_IOW(TILOAD_IOC_MAGIC, 5, int)
#define TILOAD_IOCTL_SET_CALIBRATION	_IOW(TILOAD_IOC_MAGIC, 7, int)

#define RESULT_PASS			(0x00)
#define RE_FAIL_HI			(0x01)
#define RE_FAIL_LO			(0x10)
#define RE_CHK_MSK			(0x11)
#define MAX_BIN_SIZE			(1024)

#define TASDEVICE_MAX_CHANNELS	8

#define TAS2563_MAX_DEVICE	4

struct smartpa_info	{
	unsigned char dev_name[32];
	unsigned char spkvendorid;
	unsigned char ndev;
	unsigned char i2c_list[TASDEVICE_MAX_CHANNELS];
	unsigned char bSPIEnable;
};

uint32_t tasdevice_switch_device(struct TFTCConfiguration *ftc,
	uint8_t i2cslave);
uint32_t tasdevice_coeff_read(struct TFTCConfiguration *ftc,
	uint32_t reg);
uint8_t tasdevice_byte_read(struct TFTCConfiguration *ftc,
	uint32_t reg);
void tasdevice_coeff_write(struct TFTCConfiguration *ftc,
	uint32_t reg, uint32_t data);
void tasdevice_byte_write(struct TFTCConfiguration *ftc,
	uint32_t reg, uint8_t data);
void tasdevice_save_cal(struct TFTCConfiguration *ftc,
	char *pFileName);
unsigned char check_spk_bounds(struct TFTCConfiguration *ftc,
				double re1, int spkno);
int tasdevice_load_calibration(struct TFTCConfiguration *ftc,
	int nCalibration);
int tasdevice_open_bin(struct TFTCConfiguration *ftc, unsigned char,
	char *pFileNamee);
void tasdevice_close_bin(struct TFTCConfiguration *ftc);
int InitFTCC(struct TFTCConfiguration *ftc, const struct smartpa_info *a);
int LoadFTCC(struct TFTCConfiguration *ftc);
double coeff_fixed_to_float(uint32_t coeff);
void tasdevice_set_re(struct TFTCConfiguration *ftc, double re,
	double alpha);
void tasdevice_set_temp_cal(struct TFTCConfiguration *ftc,
	uint32_t prm_PwrRMSTot, uint32_t prm_TLimit);
int tasdevice_get_f0_Q(struct TFTCConfiguration *ftc,
	double nPPC3_FWarp, double nPPC3_nFS, double nPPC3_Bl,
	double nPPC3_Mms, double *pnF0, double *pnQ);

#endif /* TASDEVICE_H_ */
