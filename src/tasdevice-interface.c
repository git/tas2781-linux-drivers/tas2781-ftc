/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2024 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "tasdevice-ftc.h"	// TAS2781 Factory Test and Calibration Tool
#include "tasdevice.h"
#include "tas2781-ftclib.h"
#include "tasdevice-common.h"

#define TAS_DEVICE		"/sys/bus/i2c/devices/2-0038"

#define TILOAD_IOC_MAGIC_PA_INFO_GET	_IOR(TILOAD_IOC_MAGIC, 34, \
	struct smartpa_info)

#define CALIBRATION_COUNT	(5)

#define DEFAULT_ENV_TEMPERATURE	(20.0)

enum spk_vendor_list {
	OTHER_SPK_VND,
	GTK,
	SW,
};

struct tidevice {
	int mPAdev;
	struct TFTCConfiguration sFTCC;
};

struct tasdev_buf {
	short real_size;
	short used_size;
	unsigned char *buf;
};

int wr_nd(const char *kcontrol, const char *node, const char *value);
int rd_nd(const char *kcontrol, const char *node, char *value, int len);

struct smartamp_with_dsp_lib {
	struct tidevice *(*smartamp_init)(char *cnt_file_name);
	void (*smartamp_deinit)(struct tidevice *TIdev);
	int (*smartamp_calibrate)(void);
	int (*smartamp_get_r0)(struct tidevice *TIdev, unsigned int *r0_array);
	int (*smartamp_get_re)(struct tidevice *TIdev, unsigned int *re_array);
	int (*smartamp_get_f0)(struct tidevice *TIdev, unsigned int *f0_array);
	int (*smartamp_get_temprature)(struct tidevice *TIdev,
		int *temprature_array);
	void (*smartamp_reg_dump)(struct tidevice *TIdev);
	int (*parser_coef_range)(struct tidevice *TIdev, char *product_name,
		double *coef_range);
	int (*set_smartamp_with_dsp_algo_scene)(unsigned int algo_scene);
	int (*smartamp_select_scene)(unsigned int reg_scene);
	void (*smartamp_calib_start)(struct tidevice *TIdev);
	void (*smartamp_calib_stop)(struct tidevice *TIdev);
	void (*smartamp_set_calib_value)(void *param, unsigned int param_len);
	void (*smartamp_with_dsp_algo_bypass)(bool bypass);
};

void smartamp_deinit(struct tidevice *TIdev)
{
	struct TFTCConfiguration *pFTCC;
	int i;

	if (!TIdev)
		return;
	pFTCC = &(TIdev->sFTCC);
	if (TIdev->mPAdev > 0) {
		close(TIdev->mPAdev);
		TIdev->mPAdev = -1;
		pFTCC->mTILoad = -1;
		if (pFTCC->mtCalBin.mBinFile)
			fclose(pFTCC->mtCalBin.mBinFile);
		if (pFTCC->mnTFctResult)
			free(pFTCC->mnTFctResult);
		if (pFTCC->nTSpkCharDev) {
			struct TSPKCharData *p = pFTCC->nTSpkCharDev;

			for (i = 0; i < pFTCC->nActiveSpk_num; i++)
				free(p->tasdev_calibrated_data);
			free(pFTCC->nTSpkCharDev);
			pFTCC->nTSpkCharDev = NULL;
		}
		if (pFTCC->mtCalBin.pBin) {
			free(pFTCC->mtCalBin.pBin);
			pFTCC->mtCalBin.pBin = NULL;
		}
		if (pFTCC->mtCalBin.pDevBlock) {
			free(pFTCC->mtCalBin.pDevBlock);
			pFTCC->mtCalBin.pDevBlock = NULL;
		}
	}
	free(TIdev);
}

struct tidevice *smartamp_init(char *cnt_file_name)
{
	struct TFTCConfiguration *pFTCC ;
	struct TFCTBinFile *pTCalBin;
	struct tidevice *TIdev;
	struct smartpa_info a;
	int ret = 0;

	TIdev = (struct tidevice *)calloc(1, sizeof(struct tidevice));
	if (!TIdev) {
		LOGE("%s: TIdev: calloc memory error\n", __func__);
		goto out;
	}

	memset(&(TIdev->sFTCC), 0x0, sizeof(struct TFTCConfiguration));
	memset(&a, 0x0, sizeof(struct smartpa_info));
	TIdev->mPAdev = open(TILOAD_NODE, O_RDWR);
	if (TIdev->mPAdev < 0) {
		LOGE("%s: Can't open i2c bus:%s\n", __func__, TILOAD_NODE);
		ret = -1;
		goto out;
	}

	pFTCC = &(TIdev->sFTCC);
	pTCalBin = &(pFTCC->mtCalBin);
	pFTCC->mTILoad = TIdev->mPAdev;

	if (cnt_file_name != NULL) {
		if (strstr(cnt_file_name, "GD_GD_GD_GD") != NULL) {
			a.spkvendorid = SW;
			LOGI("%s: spk verndor is SW!\n", __func__);
		} else {
			a.spkvendorid = GTK;
		}
	} else {
		a.spkvendorid = OTHER_SPK_VND;
	}

	pTCalBin->pBin = (char *)calloc(MAX_BIN_SIZE, sizeof(char));
	if (pTCalBin->pBin == NULL) {
		LOGE("%s: pTCalBin->pBin: calloc memory error\n", __func__);
		ret = -1;
		goto out;
	}
	pTCalBin->pDevBlock = (char *)calloc(MAX_BIN_SIZE/2, sizeof(char));
	if (pTCalBin->pDevBlock == NULL) {
		LOGE("%s: pTCalBin->pDevBlock: calloc memory error\n",
			__func__);
		ret = -1;
		goto out;
	}
	pFTCC->temp_cal = DEFAULT_ENV_TEMPERATURE;

	ret = ioctl(pFTCC->mTILoad, TILOAD_IOC_MAGIC_PA_INFO_GET, &a);
	if (ret < 0 || a.ndev == 0 || a.ndev > TASDEVICE_MAX_CHANNELS) {
		LOGE("%s: ret = %d spkno=%d TILOAD_IOC_MAGIC_PA_INFO_GET "
			"failed(%s)!\r\n", __func__, ret, a.ndev,
			strerror(errno));
		goto out;
	}

	/*
	 * tas2563 have 4 different i2c addresses
	 */
	if ((!strcmp((char *)a.dev_name, "tas2563") &&
		a.ndev > TAS2563_MAX_DEVICE)) {
		ret = -1;
		goto out;
	}

	ret = InitFTCC(pFTCC, &a);
	if (ret < 0)
		goto out;

	pFTCC->bLoadCalibration = true;

	ret = LoadFTCC(pFTCC);
	if (ret < 0)
		goto out;

out:
	if (ret) {
		if (TIdev) {
			smartamp_deinit(TIdev);
			TIdev = NULL;
		}
	}
	return TIdev;
}

int smartamp_parser_coef_range(struct tidevice *TIdev,
	char *product_name, double *coef_range)
{
	struct TFTCConfiguration *pFTCC = &(TIdev->sFTCC);
	int ret = 0, i = 0;

	if (coef_range == NULL) {
		ret = -1;
		goto EXIT;
	}

	if (product_name)
		LOGI("product_name: %s \r\n", product_name);
	else
		LOGI("product_name is NULL \r\n");


	for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
		coef_range[i*6 + 0] = pFTCC->nTSpkCharDev[i].nReLo;
		coef_range[i*6 + 1] = pFTCC->nTSpkCharDev[i].nReHi;
		coef_range[i*6 + 2] = pFTCC->nTSpkCharDev[i].nQLo;
		coef_range[i*6 + 3] = pFTCC->nTSpkCharDev[i].nQHi;
		coef_range[i*6 + 4] = pFTCC->nTSpkCharDev[i].nf0Lo;
		coef_range[i*6 + 5] = pFTCC->nTSpkCharDev[i].nf0Hi;
	}
EXIT:
	return ret;
}

int smartamp_get_r0(struct tidevice *TIdev, unsigned int *r0_array)
{
	int ret = 0;
	if (r0_array != NULL) {
		struct TFTCConfiguration *pFTCC = &(TIdev->sFTCC);
		struct TSPKCharData *nTSpkCharDev = pFTCC->nTSpkCharDev;
		struct TFCTResult *nTFctResult = pFTCC->mnTFctResult;
		double nDevARe, nDevADeltaT;
		unsigned int i = 0;

		for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
			// Default Re
			nTFctResult[i].dev_re = nTSpkCharDev[i].nPPC3_Re0;
			tasdevice_switch_device(pFTCC, nTSpkCharDev[i].nDevAddr);
			pFTCC->ftc_start(pFTCC, i);
		}
		usleep(pFTCC->nCalibrationTime*1000);
		for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
			r0_array[i] = 0;
			tasdevice_switch_device(pFTCC,
				nTSpkCharDev[i].nDevAddr);

			/* Get actual Re from TAS2781 */
			ret = pFTCC->get_Re_deltaT(pFTCC,
				nTSpkCharDev[i].nSpkReAlpha,
				&nDevARe, &nDevADeltaT);

			if (nDevARe != 0x7fffffff)
				r0_array[i] = (double)nDevARe*0x80000;
			else
				r0_array[i] = nDevARe;
		}
		for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
			tasdevice_switch_device(pFTCC,
				pFTCC->nTSpkCharDev[i].nDevAddr);
			pFTCC->ftc_stop(pFTCC, i);
		}
	} else
		ret = -1;
	return ret;
}

int smartamp_get_re(struct tidevice *TIdev, unsigned int *re_array)
{
	int ret = 0;
	if (re_array != NULL) {
		struct TFTCConfiguration *pFTCC = &(TIdev->sFTCC);
		struct TFCTResult *nTFctResult = pFTCC->mnTFctResult;

		unsigned int i = 0;
		for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
			re_array[i] = 0;
			if (nTFctResult[i].dev_re != 0x7fffffff)
				re_array[i] =
					(unsigned int)(nTFctResult[i].dev_re *
					0x80000);
			else
				re_array[i] = nTFctResult[i].dev_re;
		}
	} else
		ret = -1;
	return ret;
}

int smartamp_get_f0(struct tidevice *TIdev, unsigned int *f0_array)
{
	int ret = 0;
	if (f0_array != NULL) {
		struct TFTCConfiguration *pFTCC = &(TIdev->sFTCC);
		struct TSPKCharData *nTSpkCharDev = pFTCC->nTSpkCharDev;
		double nDevAF0, nDevAQ;
		unsigned int i = 0;

		for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
			f0_array[i] = 0;
			tasdevice_switch_device(pFTCC,
				nTSpkCharDev[i].nDevAddr);

			ret = tasdevice_get_f0_Q(pFTCC,
				nTSpkCharDev[i].nPPC3_FWarp, pFTCC->nPPC3_FS,
				nTSpkCharDev[i].nPPC3_Bl,
				nTSpkCharDev[i].nPPC3_Mms, &nDevAF0, &nDevAQ);
			if (ret < 0) {
				LOGE("ERROR:%s: failed rc = %d!\n",
					__func__, ret);
				continue;
			}
			if (nDevAF0 > 0 || nDevAF0 < 4000) {
				f0_array[i] =
					(unsigned int)(nDevAF0 * 0x80000);
			}
		}
	} else
		ret = -1;
	return ret;
}

void smartamp_reg_dump(struct tidevice *TIdev)
{
	int i = 0, rc = 0;
	struct TFTCConfiguration *pFTCC = &(TIdev->sFTCC);
	char cmd[32];
	char *buf = (char *)calloc(4096, sizeof(char));

	if (buf == NULL) {
		LOGE("ERROR:%s: calloc memory error!\n", __func__);
		return;
	}
	for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
		snprintf(cmd, sizeof(cmd), "%d 0x00 0x00", i);
		rc = wr_nd(TAS_DEVICE, "regdump", cmd);
		if (rc < 0) {
			LOGE("ERROR:%s:write regdump failed, rc = %d!\n",
				__func__, rc);
			continue;
		}

		rc = rd_nd(TAS_DEVICE, "regdump", buf, 4096);
		if (rc < 0) {
			LOGE("ERROR:%s: read redump failed, rc = %d!\n",
				__func__, rc);
			continue;
		}
	}
	free(buf);
}

void smartamp_calib_start(struct tidevice *TIdev)
{
	struct TFTCConfiguration *pFTCC = &(TIdev->sFTCC);
	struct TFCTResult *nTFctResult = pFTCC->mnTFctResult;
	struct TSPKCharData *nTSpkCharDev = pFTCC->nTSpkCharDev;
	int i = 0;

	LOGE("%s, enter\n", __func__);
	/* STEP 1: Play silence externally */

	/* STEP 2: start calibration process */
	for (i = 0; i < pFTCC->nActiveSpk_num; i++) {
		// Default Re
		nTFctResult[i].dev_re = nTSpkCharDev[i].nPPC3_Re0;
		tasdevice_switch_device(pFTCC, nTSpkCharDev[i].nDevAddr);
		pFTCC->ftc_start(pFTCC, i);
	}
	/* STEP 3: Wait for algorithm to converge */
	usleep(pFTCC->nCalibrationTime * 1000);

	return;
}

// ----------------------------------------------------------------------------
// calc_prm_pow
// ----------------------------------------------------------------------------
// Description:
//	  Calculates total rms power coefficient
//
// Inputs:
//	  re and delta_t_max
//
// Outputs:
//	  prm_pow
// ----------------------------------------------------------------------------
static uint32_t calc_prm_pow(double re, double delta_t_max, double nRTV,
	double nRTM, double nRTVA, double nSysGain)
{
	uint32_t prm_PwrRMSTot;
	double powerLimit;
	double pwrRootTot;

	//Calculate power limit
	powerLimit = delta_t_max / ((nRTV + nRTM) * nRTVA /
		((nRTV + nRTM) + nRTVA));

	pwrRootTot = sqrt(re*powerLimit) / nSysGain;

	pwrRootTot = pwrRootTot / 32;

	// Convert to fixed point
	if (pwrRootTot >= 1)
		prm_PwrRMSTot = 0x7FFFFFFF;
	else
		prm_PwrRMSTot = pwrRootTot * 0x80000000;

	return prm_PwrRMSTot;
}

// ------------------------------------------------------------------------
// Description:
//	  Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//	  executed once during production line test.
// ------------------------------------------------------------------------
void smartamp_calib_stop(struct tidevice *TIdev)
{
	struct TFTCConfiguration *ftc = &(TIdev->sFTCC);
	struct TFCTResult *nTFctResult = ftc->mnTFctResult;
	struct TSPKCharData *nTSpkCharDev = ftc->nTSpkCharDev;
	int i = 0;
	int result = 0;
	char pHint[256];

	if (TIdev->mPAdev <= 0 || nTFctResult == NULL
		|| nTSpkCharDev == NULL) {
		LOGE("%s: Pls init first!\n", __func__);
		goto out;
	}
	if (ftc->mTILoad <= 0) {
		LOGE("%s: Pls start calibration first!\n", __func__);
		goto out;
	}
	/* STEP 4: Get actual Re from TASDEVICE */
	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		tasdevice_switch_device(ftc, nTSpkCharDev[i].nDevAddr);
		nTFctResult[i].dev_re = ftc->get_re(ftc);
		LOGI("GET_RE:%lf \n", nTFctResult[i].dev_re);
	}

	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		/* STEP 5: check speaker bounds */
		nTFctResult[i].mCalbirationResult = check_spk_bounds(ftc,
			nTFctResult[i].dev_re, i);
		/* STEP 6: Set temperature limit to target TMAX */
		if ((nTFctResult[i].mCalbirationResult & RE_CHK_MSK) ==
			RESULT_PASS) {
			tasdevice_switch_device(ftc, nTSpkCharDev[i].nDevAddr);
			nTFctResult[i].dev_prm_pow =
				calc_prm_pow(nTFctResult[i].dev_re,
				nTSpkCharDev[i].nSpkTMax - ftc->temp_cal,
				nTSpkCharDev[i].nPPC3_RTV,
				nTSpkCharDev[i].nPPC3_RTM,
				nTSpkCharDev[i].nPPC3_RTVA,
				nTSpkCharDev[i].nPPC3_SysGain);
			nTFctResult[i].dev_prm_tlimit =
				ftc->calc_prm_tlimit(ftc,
				&nTSpkCharDev[i]);
			tasdevice_set_re(ftc, nTFctResult[i].dev_re,
				nTSpkCharDev[i].nSpkReAlpha);
			tasdevice_set_temp_cal(ftc,
				nTFctResult[i].dev_prm_pow,
				nTFctResult[i].dev_prm_tlimit);
		}
	}

	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		tasdevice_switch_device(ftc,
			ftc->nTSpkCharDev[i].nDevAddr);
		ftc->ftc_stop(ftc, i);
	}
	/* Step 7: Stop playing */
	/* STEP 8: Save Re, and Cal Temp into a file */
	tasdevice_save_cal(ftc, TASDEVICE_CAL_TXT);
	ftc->cal_count = (ftc->cal_count + 1) % CALIBRATION_COUNT;

	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		if ((nTFctResult[i].mCalbirationResult & RE_CHK_MSK)
			== RESULT_PASS
			&& (nTFctResult[i].bWrittenintoFile == false)) {
			sprintf(pHint, TASDEVICE_CAL_BIN,
				TASDEVICE_CAL_BIN_PATH, ftc->dev_name,
				nTSpkCharDev[i].nDevAddr >> 1);
			result = tasdevice_open_bin(ftc,
				nTSpkCharDev[i].mCoeff_dev_id, pHint);
			if (result < 0)
				break;
			tasdevice_switch_device(ftc,
				nTSpkCharDev[i].nDevAddr);
			tasdevice_set_re(ftc, nTFctResult[i].dev_re,
				nTSpkCharDev[i].nSpkReAlpha);
			tasdevice_set_temp_cal(ftc,
				nTFctResult[i].dev_prm_pow,
				nTFctResult[i].dev_prm_tlimit);
			tasdevice_close_bin(ftc);
			nTFctResult[i].bWrittenintoFile = true;
		}
	}

	if (ftc->bLoadCalibration && ftc->cal_count == 0) {
		result = tasdevice_load_calibration(ftc, 0xFF);
		for (i = 0; i < ftc->nActiveSpk_num; i++) {
			nTFctResult[i].bWrittenintoFile = false;
		}
	}
out:
	LOGI("%s: Finish FTC\n", __func__);
	ftc->mTILoad = -1;
}

const static struct smartamp_with_dsp_lib ti_devctl = {
	smartamp_init,
	smartamp_deinit,
	NULL,
	smartamp_get_r0,
	smartamp_get_re,
	smartamp_get_f0,
	NULL,
	smartamp_reg_dump,
	smartamp_parser_coef_range,
	NULL,
	NULL,
	smartamp_calib_start,
	smartamp_calib_stop,
	NULL,
	NULL
};

static void example_for_calibr_get_re(struct tidevice *TIdev)
{
	unsigned int i, *temp = NULL;
	struct smartpa_info a;
	int ret = 0;

	ret = ioctl(TIdev->mPAdev, TILOAD_IOC_MAGIC_PA_INFO_GET, &a);
	if (ret < 0 || a.ndev == 0 || a.ndev > TASDEVICE_MAX_CHANNELS) {
		LOGE("%s: ret = %d spkno=%d TILOAD_IOC_MAGIC_PA_INFO_GET "
			"failed(%s)!\r\n", __func__, ret, a.ndev,
			strerror(errno));
		return;
	}

	usleep(TIdev->sFTCC.nCalibrationTime * 1500);
	/* For Multiple spks, play at least 10 sec per spk * spk_num silence
	 * during calibration. For mono spk, at least 15 sec silence during
	 * calibration.
	 */
	ti_devctl.smartamp_calib_start(TIdev);
	temp = (unsigned int *)calloc(a.ndev, sizeof(*temp));

	usleep(TIdev->sFTCC.nCalibrationTime * 1000);

	ti_devctl.smartamp_calib_stop(TIdev);

	LOGI("RE:\n");

	if(temp) {
		ti_devctl.smartamp_get_re(TIdev, temp);
		for (i = 0; i < a.ndev; i++)
			LOGI("%f\n", (double)temp[i]/(double)0x80000);
		free(temp);
	} else
		LOGE("%s: TIdev: calloc memory error\n", __func__);
}

static void example_for_get_f0(struct tidevice *TIdev)
{
	unsigned int i, *temp = NULL;
	struct smartpa_info a;
	int ret = 0;

	ret = ioctl(TIdev->mPAdev, TILOAD_IOC_MAGIC_PA_INFO_GET, &a);
	if (ret < 0 || a.ndev == 0 || a.ndev > TASDEVICE_MAX_CHANNELS) {
		LOGE("%s: ret = %d spkno=%d TILOAD_IOC_MAGIC_PA_INFO_GET "
			"failed(%s)!\r\n", __func__, ret, a.ndev,
			strerror(errno));
		return;
	}

	temp = (unsigned int *)calloc(a.ndev, sizeof(*temp));
	if (!temp) {
		LOGE("%s: TIdev: calloc memory error\n", __func__);
		goto out;
	}

	usleep(TIdev->sFTCC.nCalibrationTime * 2500);
	/* Play at least 15 sec pink noise during f0 reading */
	ti_devctl.smartamp_get_f0(TIdev, temp);
	LOGI("f0:\n");
out:
	if(temp) {
		for (i = 0; i < a.ndev; i++)
			LOGI("%f\n", (double)temp[i]/(double)0x80000);
		free(temp);
	}
}

static void example_for_load_calibrated_data(struct tidevice *TIdev)
{
	int ret, file_size, calbin_buf_size, r_size;
	struct tasdev_buf calbin_buf = {
		.real_size = 0,
		.used_size = 0,
	};
	struct smartpa_info a;
	char calbin_name[256];
	unsigned int i;
	FILE *fp;

	ret = ioctl(TIdev->mPAdev, TILOAD_IOC_MAGIC_PA_INFO_GET, &a);
	if (ret < 0 || a.ndev == 0 || a.ndev > TASDEVICE_MAX_CHANNELS) {
		LOGE("%s: ret = %d spkno=%d TILOAD_IOC_MAGIC_PA_INFO_GET "
			"failed(%s)!\r\n", __func__, ret, a.ndev,
			strerror(errno));
		return;
	}

	for (i = 0; i < a.ndev; i++) {
		sprintf(calbin_name, TASDEVICE_CAL_BIN, TASDEVICE_CAL_BIN_PATH,
			a.dev_name, a.i2c_list[i]);
		fp = fopen(calbin_name, "rb");
		if (!fp) {
			LOGE("Cannot open configuration file for SPK %d: %s, "
				"have to use default setting\n", i,
				calbin_name);
			continue;
		}

		fseek(fp, 0L, SEEK_END);
		file_size = ftell(fp);
		fseek(fp, 0L, SEEK_SET);
		if (file_size <= 0) {
			fclose(fp);
			LOGE("configuration file for SPK %d is incorrect, "
				"have to use default setting\n", i);
			continue;
		}
		calbin_buf_size = file_size + 1;
		if (calbin_buf_size > calbin_buf.real_size) {
			if (calbin_buf.real_size)
				free(calbin_buf.buf);
			calbin_buf.buf =
				(unsigned char *)malloc(calbin_buf_size);
			if (!calbin_buf.buf) {
				LOGE("%s: TIdev: calloc memory error\n",
					__func__);
				fclose(fp);
				continue;
			}
			calbin_buf.real_size = calbin_buf_size;
		}
		calbin_buf.buf[0] = a.i2c_list[i];
		r_size = fread(&calbin_buf.buf[1], sizeof(unsigned char),
			file_size, fp);
		if (r_size > 0)
			calbin_buf.used_size = r_size + 1;
		else
			LOGE("configuration file for SPK %d read error"
				", have to use default setting\n", i);

		fclose(fp);
		ret = write(TIdev->mPAdev, calbin_buf.buf,
			calbin_buf.used_size);
		if (ret <= 0)
			LOGE("%s: dev %d: calbin load error %d\n", __func__,
				i, ret);
	}
	if (calbin_buf.buf)
		free(calbin_buf.buf);
}

static void data_conv(int val, char *data)
{
	unsigned int nByte;

	for (nByte = 0; nByte < 4; nByte++)
		data[nByte] = (val >> ((3 - nByte) * 8)) & 0xFF;

}

static void example_for_calibrated_data_query(struct tidevice *TIdev)
{
	struct TFTCConfiguration *ftc = &(TIdev->sFTCC);
	unsigned int i, val;
	char data[4];

	LOGI("%s calibrated data:\n", ftc->dev_name);
	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		LOGI("\tdev %d-I2C-0x%02x:\n", i,
			ftc->nTSpkCharDev[i].nDevAddr);
		tasdevice_switch_device(ftc, ftc->nTSpkCharDev[i].nDevAddr);
		val = tasdevice_coeff_read(ftc, ftc->r0_reg);
		data_conv(val, data);
		LOGI("\t\tBOOK0x%02xPAGE0x%02xREG0x%02x: "
			"0x%02x 0x%02x 0x%02x 0x%02x\n",
			TASDEVICE_BOOK_ID(ftc->r0_reg),
			TASDEVICE_PAGE_ID(ftc->r0_reg),
			TASDEVICE_PAGE_REG(ftc->r0_reg),
			data[0], data[1], data[2], data[3]);
		val = tasdevice_coeff_read(ftc, ftc->r0_low_reg);
		data_conv(val, data);
		LOGI("\t\tBOOK0x%02xPAGE0x%02xREG0x%02x: "
			"0x%02x 0x%02x 0x%02x 0x%02x\n",
			TASDEVICE_BOOK_ID(ftc->r0_low_reg),
			TASDEVICE_PAGE_ID(ftc->r0_low_reg),
			TASDEVICE_PAGE_REG(ftc->r0_low_reg),
			data[0], data[1], data[2], data[3]);
		val = tasdevice_coeff_read(ftc, ftc->invr0_reg);
		data_conv(val, data);
		LOGI("\t\tBOOK0x%02xPAGE0x%02xREG0x%02x: "
			"0x%02x 0x%02x 0x%02x 0x%02x\n",
			TASDEVICE_BOOK_ID(ftc->invr0_reg),
			TASDEVICE_PAGE_ID(ftc->invr0_reg),
			TASDEVICE_PAGE_REG(ftc->invr0_reg),
			data[0], data[1], data[2], data[3]);
		val = tasdevice_coeff_read(ftc, ftc->pow_reg);
		data_conv(val, data);
		LOGI("\t\tBOOK0x%02xPAGE0x%02xREG0x%02x: "
			"0x%02x 0x%02x 0x%02x 0x%02x\n",
			TASDEVICE_BOOK_ID(ftc->pow_reg),
			TASDEVICE_PAGE_ID(ftc->pow_reg),
			TASDEVICE_PAGE_REG(ftc->pow_reg),
			data[0], data[1], data[2], data[3]);
		val = tasdevice_coeff_read(ftc, ftc->tlimit_reg);
		data_conv(val, data);
		LOGI("\t\tBOOK0x%02xPAGE0x%02xREG0x%02x: "
			"0x%02x 0x%02x 0x%02x 0x%02x\n",
			TASDEVICE_BOOK_ID(ftc->tlimit_reg),
			TASDEVICE_PAGE_ID(ftc->tlimit_reg),
			TASDEVICE_PAGE_REG(ftc->tlimit_reg),
			data[0], data[1], data[2], data[3]);
	}

}

#define TILOAD_IOC_MAGIC_POWER_OFF	_IOWR(TILOAD_IOC_MAGIC, 23, struct \
	smartpa_params)
#define TILOAD_IOC_MAGIC_POWERON	_IOWR(TILOAD_IOC_MAGIC, 16, struct \
	smartpa_params)

struct smartpa_params {
	int mProg;
	int config;
	int regscene;
};

int main(int argc, char* argv[])
{
	struct smartpa_params amp_params;
	struct tidevice *TIdev;
	int rc = 0;

	if(argc != 2) {
		LOGE("%s: FTC Input param error!\n", __func__);
		return -1;
	}

	TIdev = ti_devctl.smartamp_init(NULL);

	if (!TIdev) {
		LOGE("%s: Smartpa init failed\n", __func__);
		return -1;
	}

	switch (*argv[1]) {
	case '1':
	amp_params.mProg = 0;
	amp_params.regscene = 1;
	amp_params.config = 1;
	rc = ioctl(TIdev->mPAdev, TILOAD_IOC_MAGIC_POWERON,
		&amp_params);
	if (rc) {
		LOGE("%s: Smartpa poweron failed\n", __func__);
		goto out;
	}

	example_for_calibr_get_re(TIdev);

	rc = ioctl(TIdev->mPAdev, TILOAD_IOC_MAGIC_POWER_OFF,
		&amp_params);
	if (rc) {
		LOGE("%s: Smartpa power off failed\n", __func__);
	}
		break;
	case '2':
	/* Play at least 15 sec pink noise during f0 reading */
	example_for_get_f0(TIdev);
		break;
	case '3':
	/*
	 * Read calbin and load the calibrated data into the chip.
	 * No playback needed.
	 */
	example_for_load_calibrated_data(TIdev);
		break;
	case '4':
	/*
	 * Read back calibrated data written into the chip. Before calling,
	 * playback is mandatory.
	 */
	example_for_calibrated_data_query(TIdev);
		break;
	default:
	LOGE("%c: Wrong FTC Input param!\n", *argv[1]);
		break;
	}

out:
	ti_devctl.smartamp_deinit(TIdev);

	return rc;
}
