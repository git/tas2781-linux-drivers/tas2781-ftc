/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2024 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "tasdevice-ftc.h"	// TAS2781 Factory Test and Calibration Tool
#include "tasdevice.h"
#include "tasdevice-common.h"
#include "tas2563-ftclib.h"
#include "tas2781-ftclib.h"

// ========================================================== Definitions
// Obtained from Speaker Manufacturer
// Speaker Maximum Temperature (C)
#define SPK_T_MAX		(100)
// Re +/- tolerance (%)
#define SPK_RE_TOL_PER		(10)
// Temperature coefficient alpha (1/K)
#define SPK_RE_ALPHA		(0.0039)

// Obtained from PurePath Console 3 (PPC3)
#define PPC3_RE0		(7.41)	// Re0 (ohm)
#define PPC3_FWARP		(891)
#define PPC3_BL			(0.814)
#define PPC3_MMS		(0.0666)
#define PPC3_RTV		(46.5)	// Rtv (K/W)
#define PPC3_RTM		(78.2)	// Rtm (K/W)
#define PPC3_RTVA		(2460)	// Rtva (K/W)
#define PPC3_SYSGAIN		(9.35)	// System Gain (V/FS)
#define PPC3_DEV_NONLIN_PER	(1.5)	// Device Non-linearity (%)
#define PPC3_PIG		(1)
#define COEFF_DEVICE_A		(0x03)
#define COEFF_DEVICE_B		(0x0a)
#define COEFF_DEVICE_C		(0x11)
#define COEFF_DEVICE_D		(0x15)

enum tasdev_dsp_dev_idx {
	TASDEV_DSP_TAS2559 = 4,
	TASDEV_DSP_TAS2563 = 5,
	TASDEV_DSP_TAS2781 = 10
};

static const char coeff_dev_id[] = {
	COEFF_DEVICE_A, COEFF_DEVICE_B,
	COEFF_DEVICE_C, COEFF_DEVICE_D
};

static const char *ftcfg[] = {
	"%s-a.ftcfg",
	"%s-b.ftcfg",
	"%s-c.ftcfg",
	"%s-d.ftcfg"
};


static const char *spk_vendor_name[] = {
	NULL,
	"gtk",
	"sw"
};

static int tasdevice_check_node(struct TFTCConfiguration *ftc)
{
	int ret = 0;
	if (ftc->mTILoad)
		goto out;

	ftc->mTILoad = open(TILOAD_NODE, O_RDWR);

	if (ftc->mTILoad < 0) {
		LOGE("factorytest: Can't find tiload. Please create %s.\n\r",
			TILOAD_NODE);
		ret = -1;
	}

	LOGI("factorytest: %s handle = %d\n\r", TILOAD_NODE, ftc->mTILoad);
out:
	return ret;
}

int tasdevice_load_calibration(struct TFTCConfiguration *ftc,
	int nCalibration)
{
	int ret = tasdevice_check_node(ftc);

	if (ret < 0)
		goto out;
	ret = ioctl(ftc->mTILoad, TILOAD_IOCTL_SET_CALIBRATION,
		&nCalibration);
	if (ret < 0) {
		LOGE("%s:TILOAD_IOCTL_SET_CALIBRATION failed (%s)!\n\r",
			__func__, strerror(errno));
		goto out;
	}
	usleep(500000);
out:
	return ret;
}

uint32_t tasdevice_coeff_read(struct TFTCConfiguration *ftc,
	uint32_t reg)
{
	uint8_t nbook = TASDEVICE_BOOK_ID(reg);
	uint8_t npage = TASDEVICE_PAGE_ID(reg);
	uint8_t nreg = TASDEVICE_PAGE_REG(reg);
	unsigned char pPageZero[] = {0x00, 0x00};
	unsigned char pBook[] = {0x7F, nbook};
	unsigned char pPage[] = {0x00, npage};
	unsigned char pData[] = {nreg, 0x00, 0x00, 0x00, 0x00};
	/*
	 * For reading memory-mapped registers, one extra read must be done in
	 * SPI mode. This is due to the latency of 1 frame for data from
	 * memory.
	 */
	int ret = tasdevice_check_node(ftc);

	if (ret == 0) {
		write(ftc->mTILoad, pPageZero, 2);
		write(ftc->mTILoad, pBook, 2);
		write(ftc->mTILoad, pPage, 2);
		read(ftc->mTILoad, pData, 4);
	}
	return ((pData[0] << 24) | (pData[1] << 16)
		| (pData[2] << 8) | (pData[3]));
}

uint8_t tasdevice_byte_read(struct TFTCConfiguration *ftc,
	uint32_t reg)
{
	uint8_t nbook = TASDEVICE_BOOK_ID(reg);
	uint8_t npage = TASDEVICE_PAGE_ID(reg);
	uint8_t nreg = TASDEVICE_PAGE_REG(reg);
	unsigned char pPageZero[] = {0x00, 0x00};
	unsigned char pBook[] = {0x7F, nbook};
	unsigned char pPage[] = {0x00, npage};
	unsigned char pData[] = {nreg, 0x00};
	/*
	 * For reading memory-mapped registers, one extra read must be done in
	 * SPI mode. This is due to the latency of 1 frame for data from
	 * memory.
	 */
	int ret = tasdevice_check_node(ftc);

	if (ret == 0) {
		write(ftc->mTILoad, pPageZero, 2);
		write(ftc->mTILoad, pBook, 2);
		write(ftc->mTILoad, pPage, 2);
		read(ftc->mTILoad, pData, 1);
	}
	return pData[0];
}

uint32_t tasdevice_switch_device(struct TFTCConfiguration *ftc,
	uint8_t i2cslave)
{
	int addr = i2cslave;
	int ret = tasdevice_check_node(ftc);

	ret = ioctl(ftc->mTILoad, TILOAD_IOCTL_SET_CHL, &addr);
	if (ret != 0) {
		LOGE("%s, ret = %d TILOAD_IOCTL_SET_CHL failed(%s)!\n",
			__func__, ret, strerror(errno));
	}

	return 0;
}

void tasdevice_coeff_write(struct TFTCConfiguration *ftc, uint32_t reg,
	uint32_t data)
{
	struct TFCTBinFile *pTCalBin = &(ftc->mtCalBin);
	uint8_t nbook = TASDEVICE_BOOK_ID(reg);
	uint8_t npage = TASDEVICE_PAGE_ID(reg);
	uint8_t nreg = TASDEVICE_PAGE_REG(reg);
	char *pDevBlock = pTCalBin->pDevBlock;
	unsigned int nByte = 0;
	int ret = 0;

	// if the bin file is open, write the coefficients to the bin file
	if (pTCalBin->mBinFile) {
		//if the bin file is open, write the coefficients to the bin
		//file
			unsigned int index = pTCalBin->mnDevBlockIndex * 4;

			pDevBlock[index] = 0;
			pDevBlock[index + 1] = 4;
			pDevBlock[index + 2] = 0x85;
			pDevBlock[index + 4] = nbook;
			pDevBlock[index + 5] = npage;
			pDevBlock[index + 6] = nreg;

			for (nByte = 0; nByte < 4; nByte++)
				pDevBlock[index + 7 + nByte] =
					(data >> ((3 - nByte) * 8)) & 0xFF;

			pTCalBin->mnDevBlockIndex += 3;

	} else {
		unsigned char pPageZero[] = {0x00, 0x00};
		unsigned char pBook[] = {0x7F, nbook};
		unsigned char pPage[] = {0x00, npage};
		unsigned char pData[] = {nreg, (data & 0xFF000000) >> 24,
					(data & 0x00FF0000) >> 16,
					(data & 0x0000FF00) >> 8,
					data & 0x000000FF};

		ret = tasdevice_check_node(ftc);
		if (ret < 0)
			return;

		write(ftc->mTILoad, pPageZero, 2);
		write(ftc->mTILoad, pBook, 2);
		write(ftc->mTILoad, pPage, 2);
		write(ftc->mTILoad, pData, 5);
	}
}

void tasdevice_byte_write(struct TFTCConfiguration *ftc, uint32_t reg,
	uint8_t data)
{
	int ret = 0;
	uint8_t nbook = 0, npage = 0, nreg = 0;

	nbook = TASDEVICE_BOOK_ID(reg);
	npage = TASDEVICE_PAGE_ID(reg);
	nreg = TASDEVICE_PAGE_REG(reg);

	unsigned char pPageZero[] = {0x00, 0x00};
	unsigned char pBook[] = {0x7F, nbook};
	unsigned char pPage[] = {0x00, npage};
	unsigned char pData[] = {nreg, data};

	ret = tasdevice_check_node(ftc);
	if (ret < 0)
		return;

	write(ftc->mTILoad, pPageZero, 2);
	write(ftc->mTILoad, pBook, 2);
	write(ftc->mTILoad, pPage, 2);
	write(ftc->mTILoad, pData, 2);
}

void tasdevice_save_cal(struct TFTCConfiguration *ftc,
	char *pFileName)
{
	FILE *pFile = NULL;
	int i = 0;
	struct TFCTResult *nTFctResult = ftc->mnTFctResult;
	struct TSPKCharData *nTSpkCharDev = ftc->nTSpkCharDev;

	LOGI("%s calibration values:\n\r", ftc->dev_name);

	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		LOGI("	Dev[%d] Re		= %1.2f Ohm\n\r", i,
			nTFctResult[i].dev_re);
		LOGI("	Dev[%d] rms_pow	= 0x%08X\n\r", i,
			nTFctResult[i].dev_prm_pow);
		LOGI("	Dev[%d] t_limit	= 0x%08X\n\r", i,
			nTFctResult[i].dev_prm_tlimit);

		if ((nTFctResult[i].mCalbirationResult & RE_CHK_MSK) ==
			RESULT_PASS)
			LOGI("SPK[%d] calibration success!\n\r", i);
		else {
			if (nTFctResult[i].mCalbirationResult & RE_FAIL_HI)
				LOGE("SPK[%d] Calibration fail : Re is too "
				"high (limit: %1.2f).\n\r", i,
				nTSpkCharDev[i].nReHi);
			else
				LOGE("SPK[%d] Calibration fail : Re is too "
					"low (limit: %1.2f).\n\r", i,
					nTSpkCharDev[i].nReLo);
		}
	}

	pFile = fopen(pFileName, "w+");
	if (pFile != NULL) {
		fprintf(pFile, "Ambient temperature = %2.2f\n\r\n\r",
			ftc->temp_cal);
		for (i = 0; i < ftc->nActiveSpk_num; i++) {
			fprintf(pFile, "Dev[%d] Re = %1.2f\n\r", i,
				nTFctResult[i].dev_re);
			fprintf(pFile, "Dev[%d] rms_pow	 = 0x%08X\n\r", i,
				nTFctResult[i].dev_prm_pow);
			fprintf(pFile, "Dev[%d] t_limit	 = 0x%08X\n\r", i,
				nTFctResult[i].dev_prm_tlimit);
			fprintf(pFile, "Dev[%d] Result = 0x%x\n\r\n\r", i,
				nTFctResult[i].mCalbirationResult);
		}
		fclose(pFile);
	} else
		LOGE("%s: %s open error!\n\r", __func__, pFileName);
}

#define DDC_DESCRIPTION "Calibration Data File for %s Mono//STEREO//QUAD"
#define CALIBRATION_DESCRIPTION \
	"Calibration snapshot for %s Mono//STEREO//QUAD"
#define DATA_DESCRIPTION "data blocks for %s Mono//STEREO//QUAD FTCc"

int tasdevice_open_bin(struct TFTCConfiguration *ftc,
	unsigned char CoeffDevId, char *pFileName)
{
	struct TFCTBinFile *pTCalBin = &(ftc->mtCalBin);
	char *pDevBlock = pTCalBin->pDevBlock;
	char *pBin = pTCalBin->pBin;
	time_t timestamp;
	char buf[128];
	int ret = 0;

	pTCalBin->mBinFile = fopen(pFileName, "wb");
	if (pTCalBin->mBinFile == NULL) {
		ret = -1;
		goto out;
	}

	pTCalBin->mnBinIndex = 0;

	memset(pBin, 0, MAX_BIN_SIZE);
	memset(pDevBlock, 0, MAX_BIN_SIZE/2);
	pTCalBin->mnDevBlockIndex = 0;

	//magic number
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = '5';
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = '5';
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = '5';
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = '2';
	pTCalBin->mnBinIndex += 1;

	if (pTCalBin->mnBinIndex + 16 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pTCalBin->mnBinIndex +=
		4 +	/* bypass size int */
		4 +	/* bypass checksum int */
		4 +	/* bypass PPC3 version int */
		4;	/* bypass DSP version int */

	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0;
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0;
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 1;
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0; /* driver version 0x00000100 */
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 4 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	time(&timestamp);
	pBin[pTCalBin->mnBinIndex] = (unsigned char)
		((timestamp&0xff000000)>>24);
	pTCalBin->mnBinIndex += 1;
	pBin[pTCalBin->mnBinIndex] = (unsigned char)
		((timestamp&0x00ff0000)>>16);
	pTCalBin->mnBinIndex += 1;
	pBin[pTCalBin->mnBinIndex] = (unsigned char)
		((timestamp&0x0000ff00)>>8);
	pTCalBin->mnBinIndex += 1;
	pBin[pTCalBin->mnBinIndex] = (unsigned char)
		((timestamp&0x000000ff));
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 64 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	/* DDC name */
	strcpy(&pBin[pTCalBin->mnBinIndex], "Calibration Data File");
	pTCalBin->mnBinIndex += 64;

	sprintf(buf, DDC_DESCRIPTION, ftc->dev_name);
	//LOGI("%s: %s\n", __func__, buf);
	if (pTCalBin->mnBinIndex + strlen(buf) + 1 >
		MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}

	strcpy(&pBin[pTCalBin->mnBinIndex], buf);
	pTCalBin->mnBinIndex += strlen(buf) + 1;

	pTCalBin->mnBinIndex += 4; /* device family index */
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0; /* device index */
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0; /* device index */
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0; /* device index */
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	/* dev_idx:
	 * 5: TAS2563 Mono
	 * 7: TAS2563 stereo
	 * 10: TAS2781 Mono
	 */
	pBin[pTCalBin->mnBinIndex] = ftc->dev_idx;

	pTCalBin->mnBinIndex += 1;

	pTCalBin->mnBinIndex +=
		2 +	/* num programs index */
		0 +	/* array programs index */
		2 +	/* num configurations index */
		0;	/* array configurations index */
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0x00;
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0x01; /* one calibration data block */
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 64 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	strcpy(&pBin[pTCalBin->mnBinIndex], "Calibration snapshot");
	pTCalBin->mnBinIndex += 64;
	sprintf(buf, CALIBRATION_DESCRIPTION, ftc->dev_name);
	//LOGI("%s: %s\n", __func__, buf);
	if (pTCalBin->mnBinIndex + strlen(buf) + 1 >
		MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	strcpy(&pBin[pTCalBin->mnBinIndex], buf);
	pTCalBin->mnBinIndex += strlen(buf) + 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	/* compatible program = smart amp (index 0) */
	pBin[pTCalBin->mnBinIndex] = 0x00;
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0x00; /* compatible configuration */
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 64 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	strcpy(&pBin[pTCalBin->mnBinIndex], "Calibration Data");
	pTCalBin->mnBinIndex += 64;
	sprintf(buf, DATA_DESCRIPTION, ftc->dev_name);
	//LOGI("%s: %s\n", __func__, buf);
	if (pTCalBin->mnBinIndex + strlen(buf) + 1 >
		MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	strcpy(&pBin[pTCalBin->mnBinIndex], buf);
	pTCalBin->mnBinIndex += strlen(buf) + 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 0x00;
	pTCalBin->mnBinIndex += 1;
	if (pTCalBin->mnBinIndex + 1 > MAX_BIN_SIZE) {
		ret = -1;
		goto out;
	}
	pBin[pTCalBin->mnBinIndex] = 1;	/* one blocks */
	pTCalBin->mnBinIndex += 1;

	pDevBlock[0] = 0;
	pDevBlock[1] = 0;
	pDevBlock[2] = 0;
	pDevBlock[3] = CoeffDevId;	/* COEFF_DEVICE_A - 0x03 */
	pTCalBin->mnDevBlockIndex = 2;

out:
	return ret;
}

void tasdevice_close_bin(struct TFTCConfiguration *ftc)
{
	struct TFCTBinFile *pTCalBin = &(ftc->mtCalBin);
	char *pDevBlock = pTCalBin->pDevBlock;
	char *pBin = pTCalBin->pBin;
	int rc = 0;

	/* write number of commands for calibration block */
	pDevBlock[4] = ((pTCalBin->mnDevBlockIndex-2) & 0xFF000000) >> 24;
	pDevBlock[5] = ((pTCalBin->mnDevBlockIndex-2) & 0x00FF0000) >> 16;
	pDevBlock[6] = ((pTCalBin->mnDevBlockIndex-2) & 0x0000FF00) >> 8;
	pDevBlock[7] = ((pTCalBin->mnDevBlockIndex-2) & 0x000000FF);

	memcpy(&pBin[pTCalBin->mnBinIndex], pDevBlock,
		pTCalBin->mnDevBlockIndex*4);
	pTCalBin->mnBinIndex += (pTCalBin->mnDevBlockIndex*4);

	/* write bin file size */
	pTCalBin->pBin[4] = (pTCalBin->mnBinIndex & 0xFF000000) >> 24;
	pTCalBin->pBin[5] = (pTCalBin->mnBinIndex & 0x00FF0000) >> 16;
	pTCalBin->pBin[6] = (pTCalBin->mnBinIndex & 0x0000FF00) >> 8;
	pTCalBin->pBin[7] = (pTCalBin->mnBinIndex & 0x000000FF);

	rc = fwrite(pBin, sizeof(char), pTCalBin->mnBinIndex,
		pTCalBin->mBinFile);
	if (rc != (int)pTCalBin->mnBinIndex)
		LOGE("%s: rc = %d pTCalBin->mnBinIndex = %u\n", __func__, rc,
			pTCalBin->mnBinIndex);

	fclose(pTCalBin->mBinFile);
	pTCalBin->mBinFile = NULL;
}

// -----------------------------------------------------------------------
// check_spk_bounds
// -----------------------------------------------------------------------
// Description:
//	  Checks if speaker paramters are within bounds.
// -----------------------------------------------------------------------
unsigned char check_spk_bounds(struct TFTCConfiguration *ftc,
		double re1, int i)
{
	unsigned char result = RESULT_PASS;

	if (re1 > ftc->nTSpkCharDev[i].nReHi)
		result |= RE_FAIL_HI;
	if (re1 < ftc->nTSpkCharDev[i].nReLo)
		result |= RE_FAIL_LO;
	return result;
}

int InitFTCC(struct TFTCConfiguration *ftc, const struct smartpa_info *a)
{
	int ret = 0, spkno, mem_size;

	ftc->nCalibrationTime = 2000;
	ftc->nPPC3_FS = 48000;
	strcpy((char *)ftc->dev_name, (char *)a->dev_name);
	LOGI("%s: dev name: %s\n", __func__, a->dev_name);

	if (!strcmp((char *)a->dev_name, "tas2563")) {
		ftc->ftc_start = tas2563_ftc_start;
		ftc->get_re = tas2563_get_re;
		ftc->calc_prm_tlimit = tas2563_calc_prm_tlimit;
		ftc->ftc_stop = tas2563_ftc_stop;
		ftc->get_Re_deltaT = tas2563_get_Re_deltaT;
		ftc->get_keypara = tas2563_get_keypara;
		ftc->r0_reg = TAS2563_PRM_R0_REG;
		ftc->r0_low_reg = TAS2563_PRM_R0_LOW_REG;
		ftc->invr0_reg = TAS2563_PRM_INVR0_REG;
		ftc->pow_reg = TAS2563_PRM_POW_REG;
		ftc->tlimit_reg = TAS2563_PRM_TLIMIT_REG;
		ftc->dev_idx = TASDEV_DSP_TAS2563;
		ftc->prm_Plt_Flag_reg = TAS2563_PRM_PLT_FLAG_REG;
		ftc->prm_SineGain_reg = TAS2563_PRM_SINEGAIN_REG;
		mem_size = sizeof(struct tas2563_calibrated_data);
	} else {
		ftc->ftc_start = tas2781_ftc_start;
		ftc->get_re = tas2781_get_re;
		ftc->calc_prm_tlimit = tas2781_calc_prm_tlimit;
		ftc->ftc_stop = tas2781_ftc_stop;
		ftc->get_Re_deltaT = tas2781_get_Re_deltaT;
		ftc->get_keypara = tas2781_get_keypara;
		ftc->ftcfg_version_check = tas2781_ftcfg_version_check;
		ftc->r0_reg = TAS2781_PRM_R0_REG;
		ftc->r0_low_reg = TAS2781_PRM_R0_LOW_REG;
		ftc->invr0_reg = TAS2781_PRM_INVR0_REG;
		ftc->pow_reg = TAS2781_PRM_POW_REG;
		ftc->tlimit_reg = TAS2781_PRM_TLIMIT_REG;
		ftc->dev_idx = TASDEV_DSP_TAS2781;
		ftc->prm_Plt_Flag_reg = TAS2781_PRM_PLT_FLAG_REG;
		ftc->prm_SineGain_reg = TAS2781_PRM_SINEGAIN_REG;
		ftc->prm_SineGain2_reg = TAS2781_PRM_SINEGAIN2_REG;
		mem_size = sizeof(struct tas2781_calibrated_data);
	}

	ftc->bVerbose = false;
	ftc->bLoadCalibration = false;
	ftc->nActiveSpk_num = a->ndev;
	ftc->spkvendorid = a->spkvendorid;
	ftc->bSPIEnable = a->bSPIEnable ? true : false;
	ftc->mnTFctResult = (struct TFCTResult *)
		calloc(ftc->nActiveSpk_num, sizeof(struct TFCTResult));
	if (!ftc->mnTFctResult) {
		LOGE("%s: TFCTResult memory allocate error\n", __func__);
		ret = -1;
		goto out;
	}
	ftc->nTSpkCharDev = (struct TSPKCharData *)
		calloc(ftc->nActiveSpk_num, sizeof(struct TSPKCharData));
	if (!ftc->nTSpkCharDev) {
		LOGE("%s: TSPKCharData memory allocate error\n", __func__);
		ret = -1;
		goto out;
	}
	for (spkno = 0; spkno < ftc->nActiveSpk_num; spkno++) {
		struct TSPKCharData *p = &(ftc->nTSpkCharDev[spkno]);

		ftc->mnTFctResult[spkno].bWrittenintoFile = false;
		p->nSpkTMax = SPK_T_MAX;
		p->nSpkReTolPer = SPK_RE_TOL_PER;
		p->nSpkReAlpha = SPK_RE_ALPHA;
		p->nReHi = PPC3_RE0 * 1.15;
		p->nReLo = PPC3_RE0 * 0.85;
		p->nPPC3_Re0 = PPC3_RE0;
		p->nPPC3_FWarp = PPC3_FWARP;
		p->nPPC3_Bl = PPC3_BL;
		p->nPPC3_Mms = PPC3_MMS;
		p->nPPC3_RTV = PPC3_RTV;
		p->nPPC3_RTM = PPC3_RTM;
		p->nPPC3_RTVA = PPC3_RTVA;
		p->nPPC3_SysGain = PPC3_SYSGAIN;
		p->nPPC3_DevNonlinPer = PPC3_DEV_NONLIN_PER;
		p->nPPC3_PIG = PPC3_PIG;
		p->nDevAddr = a->i2c_list[spkno] << 1;
		p->tasdev_calibrated_data = calloc(1, mem_size);
		if (!p->tasdev_calibrated_data) {
			LOGE("%s: spk %d calibrated_data mem allocate error\n",
				__func__, spkno);
			ret = -1;
			break;
		}
	}

out:
	return ret;
}

unsigned int SkipCharacter(char *pData, char cCharacter,
	unsigned int nSize)
{
	unsigned int nRIndex = 0;
	unsigned int nWIndex = 0;

	for (nRIndex = 0; nRIndex < nSize; nRIndex++) {
		if (pData[nRIndex] != cCharacter)
			pData[nWIndex] = pData[nRIndex];
		nWIndex += 1;
	}

	return nWIndex;
}

unsigned int RemoveComments(char *pData, char cCharacter,
	unsigned int nSize)
{
	unsigned int nRIndex = 0;
	unsigned int nWIndex = 0;

	for (nRIndex = 0; nRIndex < nSize; nRIndex++) {
		if (pData[nRIndex] == cCharacter)
			while ((nRIndex < nSize) && (pData[nRIndex] != '\r'))
				nRIndex += 1;
		pData[nWIndex] = pData[nRIndex];
		nWIndex += 1;
	}

	return nWIndex;
}

static void cut_string(char *dst, const char *src, unsigned start,
	unsigned end)
{
	snprintf(dst, end - start + 2, "%.*s", end - start + 1, src + start);
}

static void hex_parse(unsigned char *tasdev_reg, char *val)
{
	char *endptr, *p0, *p1;
	char tmp_buf[8];
	unsigned char num;
	int i = 0;

	p0 = val;
	p1 = strchr(p0, ',');

	while (p1) {
		cut_string(tmp_buf, val, p0 - val, p1 - val - 1);
		num = strtol(tmp_buf, &endptr, 16);

		if (*endptr != '\0')
			LOGE("Conversion failed: the input string is the invalid numbers.\n");
		else
			tasdev_reg[i] = num;

		i++;
		p0 = p1 + 1;
		p1 = strchr(p0, ',');
	}
}

void ReadValue(struct TFTCConfiguration *ftc, struct TSPKCharData *spk,
	char *ln, char *val)
{
	//char *endptr = NULL;

	if (strstr(ln, "CALIBRATION_TIME") != NULL) {
		ftc->nCalibrationTime = atoi(val);
		return;
	}
	if (strstr(ln, "FS_RATE") != NULL) {
		ftc->nPPC3_FS = atof(val);
		return;
	}
	if (strstr(ln, "SPK_T_MAX") != NULL) {
		spk->nSpkTMax = atof(val);
		return;
	}
	if (strstr(ln, "SPK_RE_TOL_PER") != NULL) {
		spk->nSpkReTolPer = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_RE_ALPHA") != NULL) {
		spk->nSpkReAlpha = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_RE0") != NULL) {
		spk->nPPC3_Re0 = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_FWARP") != NULL) {
		spk->nPPC3_FWarp = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_BL") != NULL) {
		spk->nPPC3_Bl = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_MMS") != NULL) {
		spk->nPPC3_Mms = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_RTV") != NULL) {
		spk->nPPC3_RTV = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_RTM") != NULL) {
		spk->nPPC3_RTM = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_RTVA") != NULL) {
		spk->nPPC3_RTVA = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_SYSGAIN") != NULL) {
		spk->nPPC3_SysGain = atof(val);
		return;
	}
	if (strstr(ln, "PPC3_DEV_NONLIN_PER") != NULL) {
		spk->nPPC3_DevNonlinPer = atof(val);
		return;
	}
	if (strstr(ln, "RE_HI") != NULL) {
		spk->nReHi = atof(val);
		return;
	}
	if (strstr(ln, "RE_LO") != NULL) {
		spk->nReLo = atof(val);
		return;
	}
	if (strstr(ln, "F0_HI") != NULL) {
		spk->nf0Hi = atof(val);
		return;
	}
	if (strstr(ln, "F0_LO") != NULL) {
		spk->nf0Lo = atof(val);
		return;
	}
	if (strstr(ln, "Q_HI") != NULL) {
		spk->nQHi = atof(val);
		return;
	}
	if (strstr(ln, "Q_LO") != NULL) {
		spk->nQLo = atof(val);
		return;
	}
	if (!strcmp((char *)ftc->dev_name, "tas2781")) {
		struct tas2781_calibrated_data *p =
			spk->tasdev_calibrated_data;
		if (strstr(ln, "PPC3_PIG_NEW") != NULL) {
			spk->nPPC3_PIG = atof(val);
			return;
		}
		if (strstr(ln, "PPC3_DC_RES_PER") != NULL) {
			p->nPPC3_DC_RES_PER = atof(val);
			return;
		}
		if (strstr(ln, "PPC3_TH_NONLIN_PER") != NULL) {
			p->nPPC3_TH_NONLIN_PER = atof(val);
			return;
		}
		if (strstr(ln, "PPC3_TH_RES_PER") != NULL) {
			p->nPPC3_TH_RES_PER = atof(val);
			return;
		}
		if (strstr(ln, "umg_SsmKEGCye") != NULL) {
			hex_parse(p->prm_PwrRMSTot, val);
			return;
		}
		if (strstr(ln, "iks_E0") != NULL) {
			hex_parse(p->prm_R0, val);
			return;
		}
		if (strstr(ln, "yep_LsqM0") != NULL) {
			hex_parse(p->prm_InvR0, val);
			return;
		}
		if (strstr(ln, "oyz_U0_ujx") != NULL) {
			hex_parse(p->prm_R0_low, val);
			return;
		}
		if (strstr(ln, "iks_GC_GMgq") != NULL) {
			hex_parse(p->prm_TC_TMax, val);
			return;
		}
		if (strstr(ln, "gou_Yao") != NULL) {
			hex_parse(p->prm_Thr, val);
			return;
		}
		if (strstr(ln, "kgd_Wsc_Qsbp") != NULL) {
			hex_parse(p->prm_Plt_Flag, val);
			return;
		}
		if (strstr(ln, "yec_CqseSsqs") != NULL) {
			hex_parse(p->prm_SineGain, val);
			return;
		}
		if (strstr(ln, "iks_SogkGgog2") != NULL) {
			hex_parse(p->prm_SineGain2, val);
			return;
		}
	} else {
		if (strstr(ln, "PPC3_PIG") != NULL) {
			spk->nPPC3_PIG = atof(val);
			return;
		}
	}
	/*if (strstr(ln, "DEV_ADDR") != NULL) {
		spk->nDevAddr = strtol(val, &endptr, 16);
		return;
	};*/
}

int ftcc_parse(struct TFTCConfiguration *ftc, struct TSPKCharData *pSpk,
			char *pData, unsigned int nSize)
{
	char *pLine = NULL;
	char *pEqual = NULL;
	// double nTest;

	nSize = SkipCharacter(pData, ' ', nSize);
	nSize = RemoveComments(pData, ';', nSize);

	pData[nSize] = 0;

	pLine = strtok(pData, "\n\r");

	while (pLine) {
		if (pLine[0]) {
			pEqual = strstr(pLine, "=");
			if ((pEqual) && (strlen(pEqual) > 1)) {
				pEqual[0] = 0;
				ReadValue(ftc, pSpk, pLine, pEqual + 1);
				pEqual[0] = '=';
			}
		}
		pLine = strtok(NULL, "\n\r");
	}

	return 0;
}

int LoadFTCC(struct TFTCConfiguration *ftc)
{
	struct TSPKCharData *pSpk = ftc->nTSpkCharDev;
	char ftcfile_name[64], pHint[256];
	int i, size, r_size;
	char *pData = NULL;
	int maxSize = 0;
	int ret = 0;
	FILE *pFile;

	for (i = 0; i < ftc->nActiveSpk_num; i++) {
		sprintf(ftcfile_name, ftcfg[i], ftc->dev_name );

		if(spk_vendor_name[ftc->spkvendorid])
			sprintf(pHint, "%s%s_%s", TASDEVICE_CAL_CFG_PATH,
				spk_vendor_name[ftc->spkvendorid],
				ftcfile_name);
		else
			sprintf(pHint, "%s%s", TASDEVICE_CAL_CFG_PATH,
				ftcfile_name);

		pFile = fopen(pHint, "rb");
		if (pFile == NULL) {
			LOGE("Cannot open configuration file for SPK %d: %s, "
				"have to use default setting\n", i, pHint);
			continue;
		}

		fseek(pFile, 0L, SEEK_END);
		size = ftell(pFile);
		fseek(pFile, 0L, SEEK_SET);
		if (size <= 0) {
			fclose(pFile);
			pFile = NULL;
			LOGE("configuration file for SPK %d is incorrect, "
				"have to use default setting\n", i);
			continue;
		}

		if (maxSize < size) {
			maxSize = size;
			if(pData)
				free(pData);
			pData = (char *)malloc(size);
		}

		if (pData) {
			r_size = fread(pData, sizeof(char), size, pFile);
			if (r_size <= 0) {
				fclose(pFile);
				pFile = NULL;
				LOGE("configuration file for SPK %d read error"
					", have to use default setting\n", i);
				continue;
			}
			fclose(pFile);
			pFile = NULL;
			ftcc_parse(ftc, &pSpk[i], pData, size);
			pSpk[i].mCoeff_dev_id = coeff_dev_id[i];
		} else {
			LOGE("Cannot allocate memory for configuation file "
				"for SPK %d\n", i);
			ret = -1;
			break;
		}
	}

	if (!strcmp((char *)ftc->dev_name, "tas2781")) {
		ret = ftc->ftcfg_version_check(ftc);
		LOGI("ftcfg file version is %d\n", ret);
		if (ret == 1) {
			struct tas2781_calibrated_data *p =
				pSpk[0].tasdev_calibrated_data;

			ftc->prm_Plt_Flag_reg =
				TASDEVICE_REG(p->prm_Plt_Flag[0],
				p->prm_Plt_Flag[1], p->prm_Plt_Flag[2]);
			ftc->prm_SineGain_reg =
				TASDEVICE_REG(p->prm_SineGain[0],
				p->prm_SineGain[1], p->prm_SineGain[2]);
			ftc->prm_SineGain2_reg =
				TASDEVICE_REG(p->prm_SineGain2[0],
				p->prm_SineGain2[1], p->prm_SineGain2[2]);

			ftc->prm_Thr_reg =
				TASDEVICE_REG(p->prm_Thr[0], p->prm_Thr[1],
				p->prm_Thr[2]);
			ftc->r0_reg = TASDEVICE_REG(p->prm_R0[0], p->prm_R0[1],
				p->prm_R0[2]);
			ftc->pow_reg = TASDEVICE_REG(p->prm_PwrRMSTot[0],
				p->prm_PwrRMSTot[1], p->prm_PwrRMSTot[2]);
			ftc->invr0_reg = TASDEVICE_REG(p->prm_InvR0[0],
				p->prm_InvR0[1], p->prm_InvR0[2]);
			ftc->r0_low_reg = TASDEVICE_REG(p->prm_R0_low[0],
				p->prm_R0_low[1], p->prm_R0_low[2]);
			ftc->tlimit_reg = TASDEVICE_REG(p->prm_TC_TMax[0],
				p->prm_TC_TMax[1], p->prm_TC_TMax[2]);
			ret = 0;
		}
	}
	if (pData)
		free(pData);
	if (pFile)
		fclose(pFile);

	return ret;
}

// ----------------------------------------------------------------------------
// coeff_fixed_to_float
// ----------------------------------------------------------------------------
// Description:
//	  Converts coefficients from fixed format to double.
//
// Inputs:
//	  coeff - 32-bit coefficient in 5.27 format.
// ----------------------------------------------------------------------------
double coeff_fixed_to_float(uint32_t coeff)
{
	double d_coeff;

	if ((coeff & 0x80000000) != 0)
		// 2 Complement
		d_coeff = -1 * (double)(~(coeff - 1));
	else
		d_coeff = (double)(coeff);

	// All the coeffs are 5.x
	return d_coeff / 0x08000000;
}

// ----------------------------------------------------------------------------
// set_re
// ----------------------------------------------------------------------------
// Description:
//	Writes an Re value to the TAS2563/TAS2781.
//
// Inputs:
//	PPC3_Re0 - Nominal Re obtained during characterization.
//	re - New Re value to be written to the TAS2563/TAS2781.
//	alpha - Voice coil temperature coefficient of resistance.
//	t_cal - Calibration temperature.
// ----------------------------------------------------------------------------
void tasdevice_set_re(struct TFTCConfiguration *ftc, double re,
	double alpha)
{
	uint32_t prm_R0_low;
	uint32_t prm_InvR0;
	uint32_t prm_R0;
	double r0_low;
	double inv_r0;
	double r0;

	// Calculate PRM_R0
	r0 = re / 16;
	if (r0 >= 1)
		prm_R0 = 0x7FFFFFFF;
	else
		prm_R0 = r0 * 0x80000000;

	// Calculate PRM_R0_LOW
	r0_low = re * (1 + alpha*(-60)) / 16;
	if (r0_low >= 1)
		prm_R0_low = 0x7FFFFFFF;
	else
		prm_R0_low = r0_low * 0x80000000;

	// Calculate prm_InvR0
	inv_r0 = (1.0 / re);
	if (inv_r0 >= 1)
		prm_InvR0 = 0x7FFFFFFF;
	else
		prm_InvR0 = inv_r0 * 0x80000000;

	// Write values to non-integrated tasdevice
	tasdevice_coeff_write(ftc, ftc->r0_reg, prm_R0);
	tasdevice_coeff_write(ftc, ftc->r0_low_reg, prm_R0_low);
	tasdevice_coeff_write(ftc, ftc->invr0_reg, prm_InvR0);
}

// ----------------------------------------------------------------------------
// set_temp_cal
// ----------------------------------------------------------------------------
// Description:
//	  Set Temp Calibrated values
//
// Inputs:
//	  prm_pow and prm_tlimit
// ----------------------------------------------------------------------------
void tasdevice_set_temp_cal(struct TFTCConfiguration *ftc,
	uint32_t prm_PwrRMSTot, uint32_t prm_TLimit)
{
	tasdevice_coeff_write(ftc, ftc->pow_reg, prm_PwrRMSTot);
	tasdevice_coeff_write(ftc, ftc->tlimit_reg, prm_TLimit);
}

/* XM_340 */
#define	TASDEVICE_XM_A1_REG	TASDEVICE_REG(0x64, 0x02, 0x4c)
/* XM_341 */
#define	TASDEVICE_XM_A2_REG	TASDEVICE_REG(0x64, 0x02, 0x64)

int tasdevice_get_f0_Q(struct TFTCConfiguration *ftc,
	double nPPC3_FWarp, double nPPC3_nFS, double nPPC3_Bl, double nPPC3_Mms,
	double *pnF0, double *pnQ)
{
	uint32_t reg = (!strcmp((char *)ftc->dev_name, "tas2563"))?
		TAS2563_RUNTIME_RE_REG : TAS2781_RUNTIME_RE_REG;
	double Re_dbl, a1_dbl, a2_dbl, Mms, a1_dash, a2_dash, f_out, Q_out;
	double KmKd_KmKd;	//((K - K_Dash) * (K - K_Dash))
	double KpKd_KpKd;	//((K + K_Dash) * (K + K_Dash))
	int Re_Int, a1_Int, a2_Int/*, k_Int*/;
	double Kd_Kd;	//(K_dash * K_Dash)
	double K, K_Dash, fp, gamma, Den;
	double K_K;		//(K * K)
	int nResult = 0;

	if (!pnF0 && !pnQ)
		goto end;

	if (nPPC3_nFS == 0.0) {
		nResult = -1;
		goto end;
	}

	fp = nPPC3_FWarp;
	if (fp == 0.0) {
		K = 2.0 * nPPC3_nFS / 8;
		K_Dash = 2.0 * nPPC3_nFS;
	} else {
		K = 2 * PI * fp / tan(PI * fp / (nPPC3_nFS / 8));
		K_Dash = 2 * PI * fp / tan(PI * fp / nPPC3_nFS);
	}

	Re_Int = tasdevice_coeff_read(ftc, reg);
	Re_dbl = coeff_fixed_to_float(Re_Int);
	if (Re_dbl == 0.0) {
		nResult = -2;
		goto end;
	}

	Mms = nPPC3_Mms * 0.001;
	if (Mms == 0.0) {
		nResult = -3;
		goto end;
	}

	gamma = pow(nPPC3_Bl, 2) / (Mms * Re_dbl);

	K_K = pow(K, 2);
	Kd_Kd = pow(K_Dash, 2);
	KpKd_KpKd = pow(K + K_Dash, 2);
	KmKd_KmKd = pow(K - K_Dash, 2);

	a1_Int = tasdevice_coeff_read(ftc, TASDEVICE_XM_A1_REG);
	a2_Int = tasdevice_coeff_read(ftc, TASDEVICE_XM_A2_REG);
	a1_dbl = coeff_fixed_to_float(a1_Int) * (-1.0);
	a2_dbl = coeff_fixed_to_float(a2_Int) * (-1.0);

	Den = KpKd_KpKd + (K_K - Kd_Kd) * a1_dbl + KmKd_KmKd * a2_dbl
			+ (1 - a1_dbl + a2_dbl) * gamma * K_Dash;
	if (Den == 0.0) {
		nResult = -4;
		goto end;
	}

	a1_dash = (K_K - Kd_Kd) + (K_K + Kd_Kd) *
		a1_dbl + (K_K - Kd_Kd) * a2_dbl;
	a1_dash = (2 * a1_dash) / Den;

	a2_dash = KmKd_KmKd + (K_K - Kd_Kd) * a1_dbl + KpKd_KpKd * a2_dbl
		- (1 - a1_dbl + a2_dbl) * gamma * K_Dash;
	a2_dash /= Den;

	f_out = (4 * nPPC3_nFS * nPPC3_nFS * (1 + a1_dash + a2_dash))
			/ (1 - a1_dash + a2_dash);
	if (f_out < 0) {
		nResult = -5;
		goto end;
	}
	f_out = sqrt(f_out) / (2 * PI);

	if (pnF0 != NULL)
		*pnF0 = f_out;

	if (pnQ != NULL) {
		Q_out = 2 * PI * f_out * (1 - a1_dash + a2_dash);
		Q_out = Q_out / (4 * nPPC3_nFS * (1 - a2_dash));
		*pnQ = Q_out;
	}

end:

	return nResult;
}
