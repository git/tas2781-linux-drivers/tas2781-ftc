/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2023 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef TAS2781_LIB_FTC_H_
#define TAS2781_LIB_FTC_H_

#define TAS2781_PRM_PLT_FLAG_REG	TASDEVICE_REG(0x00, 0x14, 0x38)
#define TAS2781_PRM_SINEGAIN_REG	TASDEVICE_REG(0x00, 0x14, 0x40)
#define TAS2781_PRM_SINEGAIN2_REG	TASDEVICE_REG(0x00, 0x14, 0x44)

#define TAS2781_RUNTIME_RE_REG_TF	TASDEVICE_REG(0x64, 0x62, 0x48)
#define TAS2781_RUNTIME_RE_REG		TASDEVICE_REG(0x64, 0x63, 0x44)
#define PI	3.14159
#define TAS2781_PRM_R0_REG		TASDEVICE_REG(0x00, 0x17, 0x74)
#define TAS2781_PRM_R0_LOW_REG	TASDEVICE_REG(0x00, 0x18, 0x0c)
#define TAS2781_PRM_INVR0_REG		TASDEVICE_REG(0x00, 0x18, 0x14)
#define TAS2781_PRM_POW_REG	TASDEVICE_REG(0x00, 0x13, 0x70)
#define TAS2781_PRM_TLIMIT_REG	TASDEVICE_REG(0x00, 0x18, 0x7c)
#define TAS2781_RUNTIME_LATCH_RE_REG	TASDEVICE_REG(0x00, 0x00, 0x49)

struct tas2781_calibrated_data {
	unsigned char prm_PwrRMSTot[3];
	unsigned char prm_R0[3];
	unsigned char prm_InvR0[3];
	unsigned char prm_R0_low[3];
	unsigned char prm_TC_TMax[3];
	unsigned char prm_Thr[3];
	unsigned char prm_Plt_Flag[3];
	unsigned char prm_SineGain[3];
	unsigned char prm_SineGain2[3];

	double nPPC3_DC_RES_PER;
	double nPPC3_TH_NONLIN_PER;
	double nPPC3_TH_RES_PER;

	uint32_t prm_Thr_val;
	uint32_t mprm_SineGain2;
	uint8_t prm_int_msk;
	uint8_t mprm_CLK_CFG; //Efficiency Mode
	uint8_t mprm_Rsvd; //FORCE_EN_CLASSD_HBD
	uint8_t mprm_testPage57; //CLASSD_TOP_TM_PVDD_DMIN_VAL_1_0
	uint8_t mprm_testPage62; //DIG_CLASSD_TOP_TM_DMIN_SLOW
	uint8_t mprm_PVDD_UVLO; //PVDD_THRESHOLD_VAL_REG
	uint8_t mprm_CHNL_0; //Efficiency Mode
	uint8_t mprm_NG_CFG0; //NG_CFG0
	uint8_t mprm_IDLE_CH_DETECT; //IDLE_CH_DETECT
};

void tas2781_ftc_start(struct TFTCConfiguration *ftc, int spkno);
double tas2781_get_re(struct TFTCConfiguration *ftc);
void tas2781_ftc_stop(struct TFTCConfiguration *ftc, int spkno);

/* below functions are used in SPK measurement only */
int tas2781_get_Re_deltaT(struct TFTCConfiguration *ftc,
	double nPPC3_alpha,	double *pnRe, double *pnDeltaT);
void tas2781_get_keypara(struct TFTCConfiguration *ftc);
uint32_t tas2781_calc_prm_tlimit(struct TFTCConfiguration *ftc,
	struct TSPKCharData *spk);
int tas2781_ftcfg_version_check(struct TFTCConfiguration *ftc);
#endif /* TAS2781_LIB_FTC_H_ */
