/*
 * TAS2563/TAS2781 ftc
 *
 * Copyright (C) 2022 - 2023 Texas Instruments Incorporated
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "tasdevice-ftc.h"
#include "tasdevice.h"
#include "tasdevice-common.h"
#include "tas2563-ftclib.h"

#define TAS2563_PRM_ENFF_REG		TASDEVICE_REG(0x00, 0x0d, 0x54)
#define TAS2563_PRM_DISTCK_REG		TASDEVICE_REG(0x00, 0x0d, 0x58)
#define TAS2563_PRM_TE_SCTHR_REG	TASDEVICE_REG(0x00, 0x0f, 0x60)
/* prm_Int_B0 */
#define TAS2563_TE_TA1_REG		TASDEVICE_REG(0x00, 0x10, 0x0c)
/* prm_Int_A1 */
#define TAS2563_TE_TA1_AT_REG		TASDEVICE_REG(0x00, 0x10, 0x10)
/* prm_TE_Beta */
#define TAS2563_TE_TA2_REG		TASDEVICE_REG(0x00, 0x0f, 0x64)
/* prm_TE_Beta1 */
#define TAS2563_TE_AT_REG		TASDEVICE_REG(0x00, 0x0f, 0x68)
/* prm_TE_1_Beta1 */
#define TAS2563_TE_DT_REG		TASDEVICE_REG(0x00, 0x0f, 0x70)

void tas2563_ftc_start(struct TFTCConfiguration *ftc, int spkno)
{
	struct TSPKCharData *spk = &(ftc->nTSpkCharDev[spkno]);
	struct tas2563_calibrated_data *p = spk->tasdev_calibrated_data;
	uint32_t te_TA1_AT = 0x0036915e; /* 100 ms*/
	uint32_t te_TA1 = 0x0036915e; /* 100 ms*/
	uint32_t te_TA2 = 0x0006d372; /* 100 ms*/
	uint32_t te_AT = 0x0036915e; /* 100 ms*/
	uint32_t te_DT = 0x0036915e; /* 100 ms*/
	uint32_t prm_TE_SCThr = 0x7fffffff;
	uint32_t prm_Plt_Flag = 0x40000000;
	uint32_t prm_SineGain = 0x0a3d70a4;
	uint32_t prm_DisTck = 0x40000000;
	uint32_t prm_EnFF = 0x40000000;

	// FB Enable (prm_EnFF)
	p->mprm_EnFF = tasdevice_coeff_read(ftc, TAS2563_PRM_ENFF_REG);
	// FTE = Off (prm_DisTck)
	p->mprm_DisTck = tasdevice_coeff_read(ftc, TAS2563_PRM_DISTCK_REG);
	// SCE = Off (prm_TE_SCThr)
	p->mprm_TE_SCThr = tasdevice_coeff_read(ftc,
		TAS2563_PRM_TE_SCTHR_REG);
	// PE Enable = On (prm_Plt_Flag)
	spk->mprm_Plt_Flag = tasdevice_coeff_read(ftc,
		ftc->prm_Plt_Flag_reg);
	// PTG = 0.8 (prm_SineGain)
	spk->mprm_SineGain = tasdevice_coeff_read(ftc,
		ftc->prm_SineGain_reg);
	// TA1 in Temperature estimator
	p->mTE_TA1 = tasdevice_coeff_read(ftc, TAS2563_TE_TA1_REG);
	// TA1_AT in Temperature estimator
	p->mTE_TA1_AT = tasdevice_coeff_read(ftc, TAS2563_TE_TA1_AT_REG);
	// TA2 in Temperature estimator
	p->mTE_TA2 = tasdevice_coeff_read(ftc, TAS2563_TE_TA2_REG);
	// AT in Temperature estimator
	p->mTE_AT = tasdevice_coeff_read(ftc, TAS2563_TE_AT_REG);
	// DT in Temperature estimator
	p->mTE_DT = tasdevice_coeff_read(ftc, TAS2563_TE_DT_REG);

	// FB Enable = Off (prm_EnFF)
	tasdevice_coeff_write(ftc, TAS2563_PRM_ENFF_REG, prm_EnFF);
	// FTE = Off (prm_DisTck)
	tasdevice_coeff_write(ftc, TAS2563_PRM_DISTCK_REG, prm_DisTck);
	// SCE = Off (prm_TE_SCThr)
	tasdevice_coeff_write(ftc, TAS2563_PRM_TE_SCTHR_REG, prm_TE_SCThr);
	// PE Enable = On (prm_Plt_Flag)
	tasdevice_coeff_write(ftc, ftc->prm_Plt_Flag_reg, prm_Plt_Flag);
	// PTG = 0.8 (prm_SineGain) peak -22dBfs
	tasdevice_coeff_write(ftc, ftc->prm_SineGain_reg, prm_SineGain);

	// TA1 in Temperature Estimator set to 100ms
	tasdevice_coeff_write(ftc, TAS2563_TE_TA1_REG, te_TA1);
	// TA1_AT in Temperature Estimator set to 100ms
	tasdevice_coeff_write(ftc, TAS2563_TE_TA1_AT_REG, te_TA1_AT);
	// TA2 in Temperature Estimator set to 100ms
	tasdevice_coeff_write(ftc, TAS2563_TE_TA2_REG, te_TA2);
	// AT in Temperature Estimator set to 100ms
	tasdevice_coeff_write(ftc, TAS2563_TE_AT_REG, te_AT);
	// DT in Temperature Estimator set to 100ms
	tasdevice_coeff_write(ftc, TAS2563_TE_DT_REG, te_DT);
}

/*
XM_BOOK = 0x64
XM_REGISTER = (XM_IND - (FLOOR(XM_IND /30, 1)*30)) * 4 + 8;
XM_PAGE = FLOOR(XM_IND /30,1)+1
*/

/* XM_338*/
#define TAS2563_RUNTIME_RE_REG_TF	TASDEVICE_REG(0x64, 0x02, 0x70)
// -----------------------------------------------------------------------------
// get_re
// -----------------------------------------------------------------------------
// Description:
//      Used to measure actual Re from the speaker.
//
// Inputs:
//      gpFTCC->nPPC3_Re0 - Nominal Re obtained during characterization.
// -----------------------------------------------------------------------------
double tas2563_get_re(struct TFTCConfiguration *ftc)
{
	uint32_t prm_R0;
	double re;

	// Determine whether the speakers are connected?
	prm_R0 = tasdevice_coeff_read(ftc, TAS2563_RUNTIME_RE_REG_TF);

	if (prm_R0 & 0x80000000)
		re = 0x7fffffff;
	else {
		//Get re value
		prm_R0 = tasdevice_coeff_read(ftc, TAS2563_RUNTIME_RE_REG);
		re = coeff_fixed_to_float(prm_R0);
	}

	return re;
}

void tas2563_ftc_stop(struct TFTCConfiguration *ftc, int spkno)
{
	struct TSPKCharData *spk = &(ftc->nTSpkCharDev[spkno]);
	struct tas2563_calibrated_data *p = spk->tasdev_calibrated_data;

	// FB Enable  (prm_EnFF)
	tasdevice_coeff_write(ftc, TAS2563_PRM_ENFF_REG,
		p->mprm_EnFF);
	// FTE (prm_DisTck)
	tasdevice_coeff_write(ftc, TAS2563_PRM_DISTCK_REG,
		p->mprm_DisTck);
	// SCE (prm_TE_SCThr)
	tasdevice_coeff_write(ftc, TAS2563_PRM_TE_SCTHR_REG,
		p->mprm_TE_SCThr);
	// PE Enable(prm_Plt_Flag)
	tasdevice_coeff_write(ftc, ftc->prm_Plt_Flag_reg,
		spk->mprm_Plt_Flag);
	// PTG (prm_SineGain)
	tasdevice_coeff_write(ftc, ftc->prm_SineGain_reg,
		spk->mprm_SineGain);
	// TA1 in Temperature Estimator
	tasdevice_coeff_write(ftc, TAS2563_TE_TA1_REG, p->mTE_TA1);
	// TA1_AT in Temperature Estimator
	tasdevice_coeff_write(ftc, TAS2563_TE_TA1_AT_REG,
		p->mTE_TA1_AT);
	// TA2 in Temperature Estimator
	tasdevice_coeff_write(ftc, TAS2563_TE_TA2_REG, p->mTE_TA2);
	// AT in Temperature Estimator
	tasdevice_coeff_write(ftc, TAS2563_TE_AT_REG, p->mTE_AT);
	// DT in Temperature Estimator
	tasdevice_coeff_write(ftc, TAS2563_TE_DT_REG, p->mTE_DT);
}

int tas2563_get_Re_deltaT(struct TFTCConfiguration *ftc,
	double nPPC3_alpha, double *pnRe, double *pnDeltaT)
{
	double r0, Re_rt, Re_cal;
	int prm_R0, prm_R0_rt;
	int nResult = 0;

	if (!pnRe && !pnDeltaT) {
		nResult = -1;
		goto end;
	}

	// Determine whether the speakers are connected?
	prm_R0 = tasdevice_coeff_read(ftc, TAS2563_RUNTIME_RE_REG_TF);
	if (prm_R0 & 0x80000000) {
		*pnRe = 0x7fffffff;
		goto end;
	}

	if (!nPPC3_alpha) {
		nResult = -2;
		goto end;
	}

	prm_R0 = tasdevice_coeff_read(ftc, TAS2563_PRM_R0_REG);
	r0 = (double)prm_R0 / 0x80000000;
	Re_cal = r0 * 16;
	if (Re_cal == 0.0) {
		nResult = -3;
		goto end;
	}

	prm_R0_rt = tasdevice_coeff_read(ftc, TAS2563_RUNTIME_RE_REG);
	Re_rt = coeff_fixed_to_float(prm_R0_rt);

	if (pnRe)
		*pnRe = Re_rt;

	if (pnDeltaT)
		*pnDeltaT = (Re_rt - Re_cal) / (Re_cal * nPPC3_alpha);

end:
	return nResult;
}

void tas2563_get_keypara(struct TFTCConfiguration *ftc)
{
	// PTG = 0.8 (prm_SineGain)
	uint32_t mprm_Plt_Flag = tasdevice_coeff_read(ftc,
		ftc->prm_Plt_Flag_reg);
	// PTG = 0.8 (prm_SineGain2)
	uint32_t mprm_SineGain = tasdevice_coeff_read(ftc,
		ftc->prm_SineGain_reg);

	LOGI("mprm_Plt_Flag:0X%08X\n", mprm_Plt_Flag);
	LOGI("mprm_SineGain:0X%08X\n", mprm_SineGain);
}

// ----------------------------------------------------------------------------
// calc_prm_tlimit
// ----------------------------------------------------------------------------
// Description:
//	  Calculates the thermal limit coefficient
//
// Inputs:
//	  delta_t_max
//
// Outputs:
//	  prm_TLimit
// ----------------------------------------------------------------------------
uint32_t tas2563_calc_prm_tlimit(struct TFTCConfiguration *ftc,
	struct TSPKCharData *spk)
{
	double delta_t_max = spk->nSpkTMax - ftc->temp_cal;
	double nDevNonlinPer = spk->nPPC3_DevNonlinPer;
	double alpha = spk->nSpkReAlpha;
	double nPIG = spk->nPPC3_PIG;
	// Thermal controller
	double TC_Temp_Device_NonLin;
	double TC_TLimit, PI_gain;
	uint32_t prm_TLimit;

	// Calculation to Thermal Limit
	TC_Temp_Device_NonLin = floor(((1 / alpha) + delta_t_max) *
		(nDevNonlinPer / 100));

	// Read PI Gain from PPC3 configuration file
	PI_gain = nPIG;

	// Calculate TC_Limit
	TC_TLimit = (delta_t_max - TC_Temp_Device_NonLin) *
		(((2 * PI_gain) + 1) / (2 * PI_gain));
	TC_TLimit = TC_TLimit / 256;

	if (TC_TLimit >= 1)
		prm_TLimit = 0x7FFFFFFF;
	else
		prm_TLimit = TC_TLimit * 0x80000000;

	return prm_TLimit;
}
